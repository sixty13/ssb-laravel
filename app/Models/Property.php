<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;
    protected $table = 'properties';
    protected $fillable = ['name','address','specification','amenity','short_id','image','floor_plan','area','country','city','state','pincode'];

    public function specification()
    {
        return $this->hasMany(Property_specification::class, 'property_id')->with('specification');
    }
    
    public function flat()
    {
        return $this->hasMany(Property_flat::class, 'property_id')->orderBy('id');
    }
  
    public function images()
    {
        return $this->hasOne(Property_image::class, 'property_id');
    }
    
    public function imageArr()
    {
        return $this->hasMany(Property_image::class, 'property_id');
    }
}
