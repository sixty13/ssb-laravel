<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\Amenity;
use App\Models\City;
use App\Models\Country;
use App\Models\Property_flat;
use App\Models\Property_specification;
use App\Models\State;
use App\Models\Specification;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Http\Request;
use NunoMaduro\Collision\Adapters\Phpunit\State as PhpunitState;
use App\Models\Property_image;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class PropertyController extends Controller
{
    public function index()
    {
        $propertyArr = Property::with('images')->orderBy('short_id')->get();
        return view('property.index', compact('propertyArr'));
    }
    public function create(Property $property)
    {
        $Specification = Specification::all();
        $amenity = Amenity::all();
        $city = City::all();
        $state = State::all();
        $country = Country::all();

        return view('property.create', compact('Specification', 'amenity', 'city', 'state', 'country'));
    }
    public function store(Property $property, Request $request)
    {
        $request->validate([
            'property_name' => 'required',
            'address' => 'required',
            'amenity' => 'required',
            'image' => 'required',
            'floor_plan' => 'required',
            'property_sqft' => 'required',
            'bed' => 'required',
            'bath' => 'required',
            'pincode' => 'required',
            'name' => 'required',
            'description' => 'required',
            'floor' => 'required',
            'sqft' => 'required',
            'bhk' => 'required',
            'status' => 'required',
            'label' => 'required',
            'amount' => 'required'
            
        ]);
        $property->amenity = implode(',', $request->amenity);
        $property->label = implode(',', $request->label);
        $property->name = $request->property_name;
        $property->address = $request->address;
        
        $property->country = $request->country; //Country::select('country_name')->where("country_id" ,  $request->country)->first()->toArray()['country_name'];
        $property->state = $request->state; //state::select('state_name')->where("state_id" ,  $request->state)->first()->toArray()['state_name'];
        $property->city =  $request->city; //City::select('city_name')->where("city_id" ,  $request->city)->first()->toArray()['city_name'];
        
        //         $imageName = [];
        
        //         foreach($request->image as $k => $ar)
        //         {
            
            //             $imageName[] = $name = time()."-{$k}.{$ar->extension()}";
            //             $ar->move(public_path('images/property'),$name);
            //         }
            //         $property->image = implode('|' , $imageName);
            
            // $floor_plan_Name = time() . '.' . $request->floor_plan->extension();
            // $request->floor_plan->move(public_path('images/floor_plan'), $floor_plan_Name);
            // $floor_plan = $floor_plan_Name;
            
            $file = $request->file('floor_plan')->getClientOriginalName();
            $property->floor_plan = file_newname(public_path('images/floor_plan/'),$file);
            $request->floor_plan->move(public_path('images/floor_plan/'),file_newname(public_path('images/floor_plan/'),$file));
           
        $property->video = $request->video;
        $property->sqft = $request->property_sqft;
        $property->amount = $request->amount;
        $property->bed = $request->bed;
        $property->bath = $request->bath;
        $property->map = $request->map;
        $property->area = $request->area;
        $property->pincode = $request->pincode;
        
        $last = Property::max('short_id');
        $property->short_id = ($last + 1);
        
        $property->save();
        $last_id =  $property->id;

        //specification
        if ($request->spe_id) {
            for ($i = 0; $i < COUNT($request->spe_id); $i++) {
                $sobj = new Property_specification;
                $sobj['name'] = $request->name[$i];
                $sobj['specification_id'] = $request->spe_id[$i];
                $sobj['description'] =  $request->description[$i];
                $sobj['property_id'] = $last_id;
                $sobj->save();
            }
        }

        //floor
        if ($request->floor) {
            for ($i = 0; $i < COUNT($request->floor); $i++) {
                $pFlate = new Property_flat;
                $pFlate['floor'] = $request->floor[$i];
                $pFlate['sqft'] = $request->sqft[$i];
                $pFlate['bhk'] = $request->bhk[$i];
                $pFlate['property_id'] = $last_id;
                $pFlate['status'] = $request->status[$i];
                $pFlate->Save();
            }
        }

        //property images
        if ($request->hasFile('image')) {
            $allowedfileExtension = ['pdf', 'jpg', 'png', 'svg'];
            $files = $request->file('image');
            foreach ($files as $file) {
                // $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);

                if ($check) {
                    // foreach ($request->image as $photo) {
                        $filename = $file->store('image');
                        Property_image::create([
                            'property_id' => $last_id,
                            'image' => $filename
                        ]);
                    // }
                }
            }
        }
        $this->sort_order(1,$last_id);
        Session::flash('message', 'Property Created Successfully...'); 
        Session::flash( 'class' ,'alert-success');
        return redirect('ssb-admn/property');
    }
    public function edit($id, Request $request)
    {
        $property = Property::with('imageArr')->find($id);

        if (isPost()) {
            $rule = [
                'property_name' => 'required',
                'address' => 'required',
                'amenity' => 'required',
                'property_sqft' => 'required',
                'bed' => 'required',
                'bath' => 'required',
                'pincode' => 'required',
                'name' => 'required',
                'description' => 'required',
                'floor' => 'required',
                'sqft' => 'required',
                'bhk' => 'required',
                'status' => 'required',
                'label' => 'required',
                'amount' => 'required'
            ];
            if(isEmptyArr($property->imageArr->toArray($rule))){
                $rule['image'] = 'required';
            }
            
            $request->validate($rule);

            $property->amenity = implode(',', $request->amenity);
            $property->label = implode(',', $request->label);
            $property->name = $request->property_name;
            $property->address = $request->address;

            $property->country = $request->country; 
            $property->state = $request->state;
            $property->city =  $request->city;
            

            $floor_plan_Name = $property->floor_plan;
            if (!empty($request->floor_plan)) {
                if (file_exists(public_path('images/floor_plan' . $property->floor_plan))) {
                    unlink(public_path('images/floor_plan' . $property->floor_plan));
                }
                $file = $request->file('floor_plan')->getClientOriginalName();
                $property->floor_plan = file_newname(public_path('images/floor_plan/'),$file);
                $request->floor_plan->move(public_path('images/floor_plan/'),$property->floor_plan);
                // $floor_plan_Name = time() . '.' . $request->floor_plan->extension();
                // $request->floor_plan->move(public_path('images/floor_plan'), $floor_plan_Name);
            }
            $property->sqft = $request->property_sqft;
            $property->amount = $request->amount;
            $property->bed = $request->bed;
            $property->bath = $request->bath;
            $property->map = $request->map;
            $property->video = $request->video;
            $property->pincode = $request->pincode;
            $property->area = $request->area;
            pr($property->toArray());
            $property->save();
            $last_id =  $property->id;

            Property_specification::where('property_id', $id)->delete();
            if ($request->spe_id) {
                for ($i = 0; $i < COUNT($request->spe_id); $i++) {
                    $sobj = new Property_specification;
                    $sobj['name'] = $request->name[$i];
                    $sobj['specification_id'] = $request->spe_id[$i];
                    $sobj['description'] =  $request->description[$i];
                    $sobj['property_id'] = $last_id;
                    $sobj->save();
                }
            }

            Property_flat::where('property_id', $id)->delete();
            if ($request->floor) {
                for ($i = 0; $i < COUNT($request->floor); $i++) {
                    $pFlate = new Property_flat;
                    $pFlate['floor'] = $request->floor[$i];
                    $pFlate['sqft'] = $request->sqft[$i];
                    $pFlate['bhk'] = $request->bhk[$i];
                    $pFlate['property_id'] = $last_id;
                    $pFlate['status'] = $request->status[$i];
                    $pFlate->save();
                }
            }

            //property images
            if ($request->hasFile('image')) {
                $allowedfileExtension = ['pdf', 'jpg', 'png', 'svg'];
                $files = $request->file('image');
                foreach ($files as $file) {
                    // $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check = in_array($extension, $allowedfileExtension);

                    if($check){
                        // foreach ($request->image as $photo) {
                            $filename = $file->store('image');
                            Property_image::create([
                                'property_id' => $last_id,
                                'image' => $filename
                            ]);
                        // }
                    }
                }
            }
            Session::flash('message', 'Property Edited Successfully...'); 
            Session::flash( 'class' ,'alert-success');
            return redirect('ssb-admn/property');
        }else{
            $Specification = Specification::all();
            $amenity = Amenity::all();
            $country = Country::all();
            $property_specification = Property_specification::where('property_id', $property->id)->get();
            $property_flat = Property_flat::where('property_id', $property->id)->get();
            return view('property.edit', compact('property', 'Specification', 'country', 'amenity', 'property_specification', 'property_flat'));
        }
    }
    public function destroy($id)
    {
        Property::where('id', $id)->delete();
        Property_specification::where('property_id', $id)->delete();
        Property_flat::where('property_id', $id)->delete();
        Session::flash('message', 'Property Deleted Successfully...'); 
        Session::flash( 'class' ,'alert-danger');
        return redirect('ssb-admn/property');
    }

    public function img_destroy(Property_image $image)
    {
        $file_path = storage_path('app') . '/' . $image->image;
        if (!file_exists($file_path)) {
            return response(['status' => 0, 'message' => 'image not found']);
        }
        unlink($file_path);
        $image->delete();
        
        return response(['status' => 1, 'message' => 'image delete successfully']);
    }
    public function sort_order($short , $productid)
    {
       
        $cal = 0;
        $Property = Property::find($productid);
        $currectShortid = $Property['short_id'];
        $where = [$currectShortid, $short];
        if($currectShortid > $short){
            $where = [$short,$currectShortid];
            $cal = 1;
        }
        $allPrperty = Property::whereBetween('short_id',$where )->orderBy('short_id','desc')->get();
        foreach($allPrperty as $k=>$v)
        {
            $NewShortid = ($cal) ? $v['short_id'] += 1 : $v['short_id'] -= 1;
            Property::find($v['id'])->update(['short_id' => $NewShortid]);
        }
        $Property->short_id = $short;
        $Property->save();
        return response(['status' => 1 , 'message' => 'updated successfully']);
    }
    public function test()
    {
        
    }
}
