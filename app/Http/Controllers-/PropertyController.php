<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\Amenity;
use App\Models\City;
use App\Models\Country;
use App\Models\Property_flat;
use App\Models\Property_specification;
use App\Models\State;
use App\Models\Specification;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Http\Request;
use NunoMaduro\Collision\Adapters\Phpunit\State as PhpunitState;

class PropertyController extends Controller
{
    public function index()
    {
        $propertyArr = Property::all();
        return view('property.index', compact('propertyArr'));
    }
    public function create(Property $property)
    {
        $Specification = Specification::all();
        $amenity = Amenity::all();
        $city = City::all();
        $state = State::all();
        $country = Country::all();

        return view('property.create', compact('Specification', 'amenity', 'city', 'state', 'country'));
    }
    
    public function store(Property $property, Request $request)
    {
        $request->validate([
            'property_name' => 'required',
            'address' => 'required',
            'amenity' => 'required',
            'image' => 'required',
            'floor_plan' => 'required',
            'property_sqft' => 'required',
            'bed' => 'required',
            'bath' => 'required',
            'pincode' => 'required',
            'name' => 'required',
            'description' => 'required',
            'floor' => 'required',
            'sqft' => 'required',
            'bhk' => 'required',
            'status' => 'required',
            'label' => 'required'
        ]);
        
        $property->amenity = implode(',', $request->amenity);
        $property->label = implode(',', $request->label);
        $property->name = $request->property_name;
        $property->address = $request->address;

       
        $property->country = Country::select('country_name')->where("country_id" ,  $request->country)->first()->toArray()['country_name'];
        $property->state = state::select('state_name')->where("state_id" ,  $request->state)->first()->toArray()['state_name'];
        $property->city =  City::select('city_name')->where("city_id" ,  $request->city)->first()->toArray()['city_name'];

        $imageName = [];
        
        foreach($request->image as $k => $ar)
        {

            $imageName[] = $name = time()."-{$k}.{$ar->extension()}";
            $ar->move(public_path('images/property'),$name);
        }
        
        $property->image = implode('|' , $imageName);

        $floor_plan_Name = time() . '.' . $request->floor_plan->extension();
        $request->floor_plan->move(public_path('images/floor_plan'), $floor_plan_Name);
        $property->floor_plan = $floor_plan_Name;

        $property->video = $request->video;
        $property->sqft = $request->property_sqft;
        $property->amount = $request->amount;
        $property->bed = $request->bed;
        $property->bath = $request->bath;
        $property->map = $request->map;
        $property->area = $request->area;
        $property->pincode = $request->pincode;
        
        $property->save();
        $last_id =  $property->id;
        if ($request->spe_id) {
            for ($i = 0; $i < COUNT($request->spe_id); $i++) {
                $sobj = new Property_specification;
                $sobj['name'] = $request->name[$i];
                $sobj['specification_id'] = $request->spe_id[$i];
                $sobj['description'] =  $request->description[$i];
                $sobj['property_id'] = $last_id;
                $sobj->save();
            }
        }
        if ($request->floor) {
            for ($i = 0; $i < COUNT($request->floor); $i++) {
                $pFlate = new Property_flat;
                $pFlate['floor'] = $request->floor[$i];
                $pFlate['sqft'] = $request->sqft[$i];
                $pFlate['bhk'] = $request->bhk[$i];
                $pFlate['property_id'] = $last_id;
                $pFlate['status'] = $request->status[$i];
                $pFlate->Save() ;
            }
        }
        return redirect('admin/property');
    }
    public function edit($id, Request $request)
    {
      
        $property = Property::find($id);

        if (isPost()) {
            $request->validate([
                'property_name' => 'required',
                'address' => 'required',
                'amenity' => 'required',
                'image' => 'required',
                'floor_plan' => 'required',
                'property_sqft' => 'required',
                'bed' => 'required',
                'bath' => 'required',
                'pincode' => 'required',
                'name' => 'required',
                'description' => 'required',
                'floor' => 'required',
                'sqft' => 'required',
                'bhk' => 'required',
                'status' => 'required',
                'label' => 'required'
            ]);
            $property->amenity = implode(',', $request->amenity);
            $property->label = implode(',', $request->label);
            $property->name = $request->property_name;
            $property->address = $request->address;

            $property->country = Country::select('country_name')->where("country_id" ,  $request->country)->first()->toArray()['country_name'];
            $property->state = state::select('state_name')->where("state_id" ,  $request->state)->first()->toArray()['state_name'];
            $property->city =  City::select('city_name')->where("city_id" ,  $request->city)->first()->toArray()['city_name'];
            
            
           if(! empty($request->image)){

                $imageName = [];
                if(! empty($property->image)){
                    $img = explode('|' ,$property->image );
                    
                        foreach($img as $ar)
                        {
                            if(file_exists(public_path('images/property/'.$ar))){
                                unlink(public_path('images/property/'.$ar));
                            }
                        }
                    
                }

                foreach($request->image as $k => $ar)
                {
                    $imageName[] = $name = time()."-{$k}.{$ar->extension()}";
                    $ar->move(public_path('images/property'),$name);

                }
                $property->image = implode('|' , $imageName);
            }

            
            if(! empty($request->floor_plan)){
                if(file_exists(public_path('images/floor_plan'.$property->floor_plan))){
                     unlink(public_path('images/floor_plan'.$property->floor_plan));
                }
                $floor_plan_Name = time() . '.' . $request->floor_plan->extension();
                $request->floor_plan->move(public_path('images/floor_plan'), $floor_plan_Name);
                $property->floor_plan = $floor_plan_Name;
            }

            $property->sqft = $request->property_sqft;
            $property->amount = $request->amount;
            $property->bed = $request->bed;
            $property->bath = $request->bath;
            $property->map = $request->map;
            $property->video = $request->video;
            $property->pincode = $request->pincode;
            $property->area = $request->area;
            

            $property->save();
            $last_id =  $property->id;

            Property_specification::where('property_id',$id)->delete();
            if ($request->spe_id) {
                for ($i = 0; $i < COUNT($request->spe_id); $i++) {
                    $sobj = new Property_specification;
                    $sobj['name'] = $request->name[$i];
                    $sobj['specification_id'] = $request->spe_id[$i];
                    $sobj['description'] =  $request->description[$i];
                    $sobj['property_id'] = $last_id;
                    $sobj->save();
                }
            }
            Property_flat::where('property_id',$id)->delete();
            if ($request->floor) {
                for ($i = 0; $i < COUNT($request->floor); $i++) {
                    $pFlate = new Property_flat;
                    $pFlate['floor'] = $request->floor[$i];
                    $pFlate['sqft'] = $request->sqft[$i];
                    $pFlate['bhk'] = $request->bhk[$i];
                    $pFlate['property_id'] = $last_id;
                    $pFlate['status'] = $request->status[$i];
                    $pFlate->save();
                }
            }
            return redirect('admin/property');
        } else {
            $Specification = Specification::all();
            $amenity = Amenity::all();
            $country = Country::all();
            $property_specification = Property_specification::where('property_id', $property->id)->get();
            $property_flat = Property_flat::where('property_id', $property->id)->get();
            return view('property.edit', compact('property', 'Specification', 'country', 'amenity', 'property_specification', 'property_flat'));
        }
    }
    public function destroy($id)
    {
        Property::where('id' , $id)->delete();
        Property_specification::where('property_id' , $id)->delete();
        Property_flat::where('property_id' , $id)->delete();
        return redirect('admin/property');

    }
}
