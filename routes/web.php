<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PropertyController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-app-cache', function() {
    Artisan::call('cache:clear');
    return "cache cleared!";
});

Route::get('/clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    return "will clear the all cached!";
});

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

// Route::get('/', function () {
//     return view('home');
// });
// Route::redirect('/', 'login');
Route::group(['prefix' => 'ssb-admn'], function (){
    Route::group(['middleware'=>['auth']],function(){
        Route::get('/',function (){
            return view('home');
        })->name('ssb-admn');
        Route::get('home',[HomeController::class ,'index']);
        Route::group(['prefix' => 'specification'], function (){
            Route::get('/' , [HomeController::class ,'specification']);
            Route::get('/create' , [HomeController::class ,'specificationForm']);
            Route::post('/store' , [HomeController::class ,'specificationCreate']);
            Route::any('/edit/{specification}' ,[HomeController::class , 'specificationEdit']);
            Route::get('/delete/{specification}' ,[HomeController::class , 'specificationDelete']);
            Route::get('/getspecification/{specification}' ,[HomeController::class , 'getSpecification']);


        });
        Route::group(['prefix' => 'amenity'], function (){
            Route::get('/' , [HomeController::class ,'amenity']);
            Route::get('/create' , [HomeController::class ,'amenityForm']);
            Route::post('/store' , [HomeController::class ,'amenityCreate']);
            Route::any('/edit/{amenity}' ,[HomeController::class , 'amenityEdit']);
            Route::get('/delete/{amenity}' ,[HomeController::class , 'amenityDelete']);
        });
        Route::group(['prefix' => 'flat'], function (){
            Route::get('/' , [HomeController::class ,'flat']);
            Route::get('/create' , [HomeController::class ,'flatForm']);
            Route::post('/store' , [HomeController::class ,'flatCreate']);
            Route::any('/edit/{flat}' ,[HomeController::class , 'flatEdit']);
            Route::get('/delete/{flat}' ,[HomeController::class , 'flatDelete']);

        });
        Route::group(['prefix' => 'property'], function (){
            Route::get('/' , [PropertyController::class ,'index']);
            Route::get('/create' , [PropertyController::class ,'create']);
            Route::post('/store' , [PropertyController::class ,'store']);
            Route::any('/edit/{Property}' ,[PropertyController::class , 'edit']);
            Route::get('/delete/{Property}' ,[PropertyController::class , 'destroy']);
            Route::get('img-delete/{image}',[PropertyController::class , 'img_destroy']);
            Route::get('sort-order/{sort}/{productid}',[PropertyController::class , 'sort_order']);
            Route::get('/test',[PropertyController::class ,'test']);
        });
    });
});

Route::get('/',[HomeController::class,'FrontHome']);
Route::get('/property-detail/{Property}',[HomeController::class ,'propertyDetail']);
Route::get('/getState/{id}',[HomeController::class ,'getState']);
Route::get('/getcity/{id}',[HomeController::class ,'getcity']);
Route::get('/property-grid',[HomeController::class ,'PropertyGrid']);
Route::get('/property-list',[HomeController::class ,'propertyList']);
Route::get('/contact-us',[HomeController::class ,'ContactUs']);
Route::post('/SubscribeUs',[HomeController::class ,'SubscribeUs']);
Route::any('/jv',[HomeController::class ,'jv']);
Route::get('/gallery',[HomeController::class ,'gallery']);
Route::get('/about-us',[HomeController::class ,'aboutUs']);
Route::get('/bunglow',[HomeController::class ,'bunglow']);
Route::post('/contact',[HomeController::class ,'contactFrom']);
Route::post('/send-mail',[HomeController::class ,'sendMail']);


Route::get('send_test_email', function(){
    Mail::raw('Sending emails with Mailgun and Laravel is easy!', function($message)
    {
        $message->subject('Mailgun and Laravel are awesome!');
        $message->from('info@sixry13.com', 'Website Name');
        $message->to('neel@sixty13.com');
    });
});

Route::get('/test-sql-injection',[HomeController::class ,'testLogin']);
// Route::get('/test/{id}/{productid}',[PropertyController::class,'sort_order']);
    
    


