{{-- <html>
    <head>
        <title>
            SSB-2
        </title>
    </head>
    <body>
        @if (session('status'))
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ session('status') }}
        </div>
        @endif
        <form method="POST" action="{{ route('login') }}">
            @csrf
            Email : <input type="email" name="email" :value="old('email')" required autofocus >
            Password : <input type="password" name="password" required autocomplete="current-password">
            <button type="submit">Submit</button>
            @if (Route::has('password.request'))
            <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                {{ __('Forgot your password?') }}
            </a>
            @endif
        </form>
    </body> 
</html> --}}


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SSB-Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('public/css/login.css')}}">
</head>
<body>

<div class="log">
    @if (session('status'))
    <div class="mb-4 font-medium text-sm text-green-600">
        {{ session('status') }}
    </div>
    @endif
    @error('email')
        <em class="isInvalid">{{ $message }}</em>
    @enderror
    <h2>Login</h2>
    <form method="POST" action="{{ route('login') }}">
        @csrf
      <div class="input-cont">
        <input type="text" name="email" placeholder="E-mail" value="{{old('email')}}" required>
        <div class="border1"></div>
      </div>
      <div class="input-cont">
        <input type="password" name="password" placeholder="Password" required autocomplete="current-password">
        <div class="border2"></div>
      </div>
      {{-- <span class="check">
        <input type="checkbox"> <label>Remember Me</label>
      </span>
      <a href="#">Forgot Password</a> --}}
      <div class="clear"></div>
      <input type="submit" value="Login">
    </form>
</div>
  
</body>
</html>