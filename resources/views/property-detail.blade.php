@extends('layouts.front')
@section('css')
<style type="text/css">
    .lightbox {
  background-color: rgba(0, 0, 0, 0.8);
  /*overflow: scroll;*/
  position: fixed;
  display: none;
  z-index: 10000000000;
  bottom: 0;
  right: 0;
  left: 0;
  top: 0;
}

.lightbox-container {
  position: relative;
  max-width: 960px;
  margin: 5% auto;
  display: block;
  padding: 0 3%;
  height: auto;
  z-index: 10;
}

@media screen and (max-width: 768px) {
  .lightbox-container {
    margin-top: 10%;
  }
}
@media screen and (max-width: 414px) {
  .lightbox-container {
    margin-top: 13%;
  }
}

.lightbox-close {
  text-transform: uppercase;
  background: transparent;
  position: absolute;
  font-weight: 300;
  font-size: 12px;
  display: block;
  border: none;
  color: white;
  top: -22px;
  right: 3%;
}

.video-container {
  padding-bottom: 56.25%;
  position: relative;
  padding-top: 30px;
  overflow: hidden;
  height: 0;
}
.video-container iframe,
.video-container object,
.video-container embed {
  position: absolute;
  height: 100%;
  width: 100%;
  left: 0;
  top: 0;
}

#playme {
  /*background: #007fed;*/
  text-transform: uppercase;
  font-weight: 300;
  border: none;
  color: white;
  padding: 10px 15px;
  display: inline-block;
  font-size: 14px;
  margin: 0;
}

</style>
@endsection
@section('content')

    <!--========== Breadcrumb -->
    <div class="bg-light">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="index.html"> <i class="fas fa-home"></i> </a></li>
                        <li class="breadcrumb-item"> <i class="fas fa-chevron-right"></i> <a href="#">Pages</a></li>
                        <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i>
                        <span> Property Details</span></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--================================= Breadcrumb -->
    <div class="wrapper">
        <!--=================================
        Property Detail -->
        <section class="mt-4"><!-- space-pt -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 mb-5 mb-lg-0 order-lg-2">
                        <div class="sticky-top">
                            <div class="mb-4">
                            <h3>{{$property->name}}</h3>
                                <span class="d-block mb-3"><i class="fas fa-map-marker-alt fa-xs pr-2"></i>{{$property->address}}</span>
                                <span class="price font-xll text-primary d-block">₹{{$property->amount}}</span>
                                <span class="sub-price font-lg text-dark d-block"><b>{{$property->sqft}}/Sqft </b> </span>
                                <ul class="property-detail-meta list-unstyled ">
                                    <li><a href="#"> <i class="fas fa-star text-warning pr-2"></i>3.9/5 </a></li>
                                    <li class="share-box">
                                        <a href="#"> <i class="fas fa-share-alt"></i> </a>
                                        <ul class="list-unstyled share-box-social">
                                            <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
                                            <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
                                            <li> <a href="#"><i class="fab fa-linkedin"></i></a></li>
                                            <li> <a href="#"><i class="fab fa-instagram"></i></a> 
                                            </li>
                                            <li> <a href="#"><i class="fab fa-pinterest"></i></a> 
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#"> <i class="fas fa-heart"></i> </a></li>
                                    <li><a href="#"> <i class="fas fa-exchange-alt"></i> </a></li>
                                    <li><a href="#"> <i class="fas fa-print"></i> </a></li>
                                </ul>
                            </div>
                            <div class="agent-contact-inner bg-dark p-4">
                                <div class="d-flex align-items-center mb-4">
                                    <div class="agent-contact-avatar mr-3">
                                        <img class="img-fluid rounded-circle avatar avatar-lg" src="{{asset('public/front/images/avatar/01.jpg')}}"
                                            alt="">
                                    </div>
                                    <div class="agent-contact-name">
                                        <h6 class="text-white mb-0">Ramya Muthukrishnan </h6>
                                        <span>Business Team</span>
                                    </div>
                                </div>
                                <div class="d-flex mb-4 align-items-center">
                                    <h6 class="text-primary border p-2 mb-0"><a href="tel:+919962482979"><i
                                                class="fas fa-phone-volume text-white pr-2"></i>+919962482979</a></h6>
                                    <a class="btn btn-link p-0 ml-auto text-white" href="#"><u>View all listing </u></a>
                                </div>
                                 <div class="alert alert-success success-msg d-none" ></strong></div>
                                <form id="contact-form">
                                    @csrf
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" placeholder="Your email Adress">
                                        <input type="hidden" class="form-control" name="property_name" value="{{$property->name}}">
                                    </div>
                                    <em class="text-danger err_msg" id="c_email"></em>    
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" placeholder="Your Name">
                                    </div>
                                    <input type="hidden" name="propertyId" value="{{request()->segment(count(request()->segments()))}}">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="phone" placeholder="Your Phone number">
                                    </div>
                                    <em class="text-danger err_msg" id="c_phone"></em>    
                                    
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" name="message" placeholder="Write Message"></textarea>
                                    </div>
                                    <em class="text-danger err_msg" id="c_message"></em>    
                                    
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label text-white font-sm" for="customCheck1">I here by
                                            agree for processing my personal data </label>
                                    </div>
                                    <div id="recaptcha" class="captcha-contact col-md-12 g-recaptcha"></div> 
                                    <em class="text-danger err_msg" id="recaptcha_msg"></em>  
                                    <a class="btn btn-primary btn-block"  id="contact_btn" href="javascript:void(0)">Send Message</a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 order-lg-1">
                        <div class="property-detail-gallery overflow-hidden">
                            <ul class="nav nav-tabs nav-tabs-02 mb-4" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link shadow active" id="photo-tab" data-toggle="pill" href="#photo"
                                        role="tab" aria-controls="photo" aria-selected="true">Photo</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link shadow" id="map-tab" data-toggle="pill" href="#map" role="tab"
                                        aria-controls="map" aria-selected="false">Map</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link shadow" id="street-map-view-tab" data-toggle="pill"
                                        href="#street-map-view" role="tab" aria-controls="street-map-view"
                                        aria-selected="false">Street view</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="photo" role="tabpanel"
                                    aria-labelledby="photo-tab">
                                    <div class="slider-slick">
                                        <div class="slider slider-for detail-big-car-gallery">
                                        
                                            @if(!isEmptyArr($property['imageArr']))
                                                @foreach ($property['imageArr'] as $item)
                                                    <img class="img-fluid" src="{{ file_exists(storage_path('app/' . $item['image'])) ? asset('storage/app/' . $item['image']) : storage_path('app/no-image.png') }}"
                                                        alt="">
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="slider slider-nav slider-nav123">
                                            @if(!isEmptyArr($property['imageArr']))
                                                @foreach ($property['imageArr'] as $item)
                                                <img class="img-fluid slider-img-nav" src="{{ file_exists(storage_path('app/' . $item['image'])) ? asset('storage/app/' . $item['image']) : storage_path('app/no-image.png')  }}" alt="" />
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="map" role="tabpanel" aria-labelledby="map-tab">
                                    <iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.8351288872545!2d144.9556518!3d-37.8173306!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1443621171568"
                                        style="border:0; width: 100%; height: 100%;"></iframe>
                                </div>
                                <div class="tab-pane fade" id="street-map-view" role="tabpanel"
                                    aria-labelledby="street-map-view-tab">
                                    <div id="street-view"></div>
                                </div>
                            </div>
                        </div>
                        <!-- ---Tab Specification--- -->
                        <div class="col-md-12 mt-lg-5 mt-md-0 box-shadow-specifiation">
                            <div class="section-title text-left">
                                <ul class="nav nav-tabs mb-4 overflow-sm" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="tab-03-tab" data-toggle="pill" href="#tab-03"
                                            role="tab" aria-controls="tab-03" aria-selected="true">Specifications</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tab-04-tab" data-toggle="pill" href="#tab-04" role="tab"
                                            aria-controls="tab-04" aria-selected="false">Floor Plan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tab-05-tab" data-toggle="pill" href="#tab-05" role="tab"
                                            aria-controls="tab-05" aria-selected="false">Available Flats</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tab-06-tab" data-toggle="pill" href="#tab-06" role="tab"
                                            aria-controls="tab-06" aria-selected="false">Address</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">

                                    <div class="tab-pane fade show active" id="tab-03" role="tabpanel"
                                        aria-labelledby="tab-03-tab">
                                        <div class="row">
                                            @foreach ($property['amenity'] as $item)
                                            <div class="col-md-6 mb-5">
                                                <div class="american">
                                                <img src="{{url('public/images/icon/'.$item['icon'])}}"  alt="american"><span>{{$item['name']}}</span>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table item-specification">
                                                <thead>
                                                    <tr>
                                                        <th style="line-height: 10px; width:35%;">Item</th>
                                                        <th style="line-height: 10px;width:65%;">Specification</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!--{{pr($property['specification']->toArray())}}-->
                                                    @foreach ($property['specification']->toArray() as $item)
                                                    <tr>
                                                        <td>
                                                            <div class="american">
                                                                <img src="{{url('public/images/icon/'.$item['specification']['icon'])}}"
                                                                    alt="american"><span>{{$item['name']}}</span>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <p>{!! $item['description'] !!}</p>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    
                                                   
                                                </tbody>


                                            </table>
                                        </div>
                                        <div>
                                            <p><strong class="spec-note">Note:</strong>Specifications can be changed as per manufacture’s availability.</p>
                                        </div>
                                        
                                    </div>

                                    <div class="tab-pane fade" id="tab-04" role="tabpanel" aria-labelledby="tab-04-tab">

                                        <p class="mt-4 text-center">Click on the Floor plan to Download the PDF
                                        <a href="javascript:void(0)" id="download_pdf" class="ml-2"><i class="fas fa-download"></i> Download</a>

                                            <!-- <embed class="mt-4 embed-r" src="http://bootstrapmonster.com/ssb/wp-content/uploads/2020/09/SARVAH-GF-PLAN.pdf" width="100%" height="1000px"></embed> -->

                                        <div id="load_iframe"></div>
                                        </p>
                                    </div>

                                    <div class="tab-pane fade" id="tab-05" role="tabpanel" aria-labelledby="tab-05-tab">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">S.NO</th>
                                                        <th scope="col">FLOOR</th>
                                                        <th scope="col">SQFT</th>
                                                        <th scope="col">BHK</th>
                                                        <th scope="col">AVAILABLE</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($property['flat'] as $k=>$item)
                                                    @php
                                                        $k += 1;
                                                    @endphp
                                                    <tr>
                                                        <th scope="row">{{$k}}</th>
                                                        <td>{{$item->floor}}</td>
                                                        <td>{{$item->sqft}}</td>
                                                        <td>{{$item->bhk}}</td>
                                                    <td><button type="button" class="btn btn-status {{($item->status == 1) ? "" : "color-green" }}">{{($item->status == 1) ? "Sold" : "Available" }}</button></td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab-06" role="tabpanel" aria-labelledby="tab-06-tab">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="address">
                                                    <span>Address:</span>
                                                    <p>
                                                       {{$property->address}}
                                                    </p>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="address">
                                                    <span>Area: </span>
                                                    <p> {{$property->area}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="address">
                                                    <span>City: </span>
                                                    <p>{{$property->city}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="address">
                                                    <span>Country: </span>
                                                    <p>{{$property->country}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="address">
                                                    <span>State: </span>
                                                    <p>{{$property->state}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="address">
                                                    <span>Pin Code: </span>
                                                    <p>{{$property->pincode}}</p>
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>

                        <!-- ---Tab Specification eND--- -->


                        <!-- video section start -->
                        <div class="box-shadow-specifiation mt-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="container">
                                        <label class="video-label">Video</label>
                                        <div class="property_video_wrapper">
                                            <div id="property_video_wrapper_player"></div>
                                             <a id="playme" onclick="revealVideo('video','youtube')" data-autoplay="true" href="javascript:void(0)" data-vbtype="video" class="venobox vbox-item">
												<img src="{{url('public/images/playicon.png')}}" alt="video image" class="w-25"></a>
                                            </a>
                                        </div>

                                        <div id="video" class="lightbox" onclick="hideVideo('video','youtube')">
                                            <div class="lightbox-container">
                                                <div class="lightbox-content">

                                                    <button onclick="hideVideo('video','youtube')" class="lightbox-close">
                                                        ✕
                                                    </button>
                                                    <div class="video-container">
                                                        <?php
                                                        $video = fetchSubStr( $property->video, "?v=", "&" );
                                                        if( !empty( $video ) ){
                                                            $video = "https://www.youtube.com/embed/".$video;
                                                        }else{
                                                            $video = str_replace('https://youtu.be','https://www.youtube.com/embed',$property->video);
                                                        }
                                                        ?>
														<iframe id="youtube" width="960" height="540" src="{{$video}}" frameborder="0" allowfullscreen> </iframe>
														<!--<iframe id="youtube" width="960" height="540" src="{{str_replace('https://youtu.be','http://www.youtube.com/embed',$property->video)}}" frameborder="0" allowfullscreen></iframe> -->
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- video section End -->
                        @if(!empty($property['map']))
                        <!-- Map section start -->
                        <div class="box-shadow-specifiation mt-5 mb-5">
                            <div class="row map-section">
                                <div class="col-md-12">
                                    <label class="video-label">Map</label><br/>
                                   {!! $property['map'] !!}
                                </div>
                            </div>
                        </div>
                        <!-- Map section End -->
                        @endif

                    </div>


                </div>
        </section>
        <!--=================================
        Property Detail -->

        <!--=================================
      Properties -->
        {{-- <section class="space-pt mb-5">
            <div class="container">
                <hr class="mb-5 mt-0">
                <h5 class="mb-4">Similar properties</h5>
                <div class="row">
                    <div class="col-md-4">
                        <div class="property-item">
                            <div class="property-image bg-overlay-gradient-04">
                                <img class="img-fluid" src="{{asset('public/front/images/property/grid/01.jpg')}}" alt="">
                                <div class="property-lable">
                                    <span class="badge badge-md badge-primary">Bungalow</span>
                                    <span class="badge badge-md badge-info">Sale </span>
                                </div>
                                <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>
                                <div class="property-agent">
                                    <div class="property-agent-image">
                                        <img class="img-fluid" src="{{asset('public/front/images/avatar/01.jpg')}}" alt="">
                                    </div>
                                    <div class="property-agent-info">
                                        <a class="property-agent-name" href="#">Alice Williams</a>
                                        <span class="d-block">Company Agent</span>
                                        <ul class="property-agent-contact list-unstyled">
                                            <li><a href="#"><i class="fas fa-mobile-alt"></i> </a></li>
                                            <li><a href="#"><i class="fas fa-envelope"></i> </a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-agent-popup">
                                    <a href="#"><i class="fas fa-camera"></i> 06</a>
                                </div>
                            </div>
                            <div class="property-details">
                                <div class="property-details-inner">
                                    <h5 class="property-title"><a href="property-detail.html">Ample apartment at last floor
                                        </a></h5>
                                    <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i>Virginia drive
                                        temple hills</span>
                                    <span class="property-agent-date"><i class="far fa-clock fa-md"></i>10 days ago</span>
                                    <div class="property-price">₹150.00<span> / month</span> </div>
                                    <ul class="property-info list-unstyled d-flex">
                                        <li class="flex-fill property-bed"><i class="fas fa-bed"></i>Bed<span>1</span></li>
                                        <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span>2</span>
                                        </li>
                                        <li class="flex-fill property-m-sqft"><i
                                                class="far fa-square"></i>sqft<span>145m</span></li>
                                    </ul>
                                </div>
                                <div class="property-btn">
                                    <a class="property-link" href="property-detail.html">See Details</a>
                                    <ul class="property-listing-actions list-unstyled mb-0">
                                        <li class="property-compare"><a data-toggle="tooltip" data-placement="top"
                                                title="Compare" href="#"><i class="fas fa-exchange-alt"></i></a></li>
                                        <li class="property-favourites"><a data-toggle="tooltip" data-placement="top"
                                                title="Favourite" href="#"><i class="far fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="property-item">
                            <div class="property-image bg-overlay-gradient-04">
                                <img class="img-fluid" src="{{asset('public/front/images/property/grid/02.jpg')}}" alt="">
                                <div class="property-lable">
                                    <span class="badge badge-md badge-primary">Apartment</span>
                                    <span class="badge badge-md badge-info">New </span>
                                </div>
                                <div class="property-agent">
                                    <div class="property-agent-image">
                                        <img class="img-fluid" src="{{asset('public/front/images/avatar/02.jpg')}}" alt="">
                                    </div>
                                    <div class="property-agent-info">
                                        <a class="property-agent-name" href="#">John doe</a>
                                        <span class="d-block">Company Agent</span>
                                        <ul class="property-agent-contact list-unstyled">
                                            <li><a href="#"><i class="fas fa-mobile-alt"></i> </a></li>
                                            <li><a href="#"><i class="fas fa-envelope"></i> </a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-agent-popup">
                                    <a href="#"><i class="fas fa-camera"></i> 12</a>
                                </div>
                            </div>
                            <div class="property-details">
                                <div class="property-details-inner">
                                    <h5 class="property-title"><a href="property-detail.html">Awesome family home</a></h5>
                                    <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i>Vermont dr.
                                        hephzibah</span>
                                    <span class="property-agent-date"><i class="far fa-clock fa-md"></i>2 months ago</span>
                                    <div class="property-price">₹326.00<span> / month</span> </div>
                                    <ul class="property-info list-unstyled d-flex">
                                        <li class="flex-fill property-bed"><i class="fas fa-bed"></i>Bed<span>2</span></li>
                                        <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span>3</span>
                                        </li>
                                        <li class="flex-fill property-m-sqft"><i
                                                class="far fa-square"></i>sqft<span>215m</span></li>
                                    </ul>
                                </div>
                                <div class="property-btn">
                                    <a class="property-link" href="property-detail.html">See Details</a>
                                    <ul class="property-listing-actions list-unstyled mb-0">
                                        <li class="property-compare"><a data-toggle="tooltip" data-placement="top"
                                                title="Compare" href="#"><i class="fas fa-exchange-alt"></i></a></li>
                                        <li class="property-favourites"><a data-toggle="tooltip" data-placement="top"
                                                title="Favourite" href="#"><i class="far fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="property-item">
                            <div class="property-image bg-overlay-gradient-04">
                                <img class="img-fluid" src="{{asset('public/front/images/property/grid/03.jpg')}}" alt="">
                                <div class="property-lable">
                                    <span class="badge badge-md badge-primary">Summer House</span>
                                    <span class="badge badge-md badge-info">Hot </span>
                                </div>
                                <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>
                                <div class="property-agent">
                                    <div class="property-agent-image">
                                        <img class="img-fluid" src="{{asset('public/front/images/avatar/03.jpg')}}" alt="">
                                    </div>
                                    <div class="property-agent-info">
                                        <a class="property-agent-name" href="#">Ramya Muthukrishnan </a>
                                        <span class="d-block">Business Team</span>
                                        <ul class="property-agent-contact list-unstyled">
                                            <li><a href="#"><i class="fas fa-mobile-alt"></i> </a></li>
                                            <li><a href="#"><i class="fas fa-envelope"></i> </a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-agent-popup">
                                    <a href="#"><i class="fas fa-camera"></i> 03</a>
                                </div>
                            </div>
                            <div class="property-details">
                                <div class="property-details-inner">
                                    <h5 class="property-title"><a href="property-detail.html">Contemporary apartment</a>
                                    </h5>
                                    <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i>Newport st.
                                        mebane, nc</span>
                                    <span class="property-agent-date"><i class="far fa-clock fa-md"></i>6 months ago</span>
                                    <div class="property-price">₹658.00<span> / month</span> </div>
                                    <ul class="property-info list-unstyled d-flex">
                                        <li class="flex-fill property-bed"><i class="fas fa-bed"></i>Bed<span>3</span></li>
                                        <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span>4</span>
                                        </li>
                                        <li class="flex-fill property-m-sqft"><i
                                                class="far fa-square"></i>sqft<span>3,189m</span></li>
                                    </ul>
                                </div>
                                <div class="property-btn">
                                    <a class="property-link" href="property-detail.html">See Details</a>
                                    <ul class="property-listing-actions list-unstyled mb-0">
                                        <li class="property-compare"><a data-toggle="tooltip" data-placement="top"
                                                title="Compare" href="#"><i class="fas fa-exchange-alt"></i></a></li>
                                        <li class="property-favourites"><a data-toggle="tooltip" data-placement="top"
                                                title="Favourite" href="#"><i class="far fa-heart"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}
        <!--=================================
      Properties -->
        <!--=================================
      Review -->

        <!--=================================
      Review -->
    </div>

   <!--==== Meet our agent -->
<section class="mt-5 mb-5">
    <div class="section-title text-center">
          <h2>Associate and Partners</h2>
    </div>
    <div class="container">
        <div class="owl-carousel-bank owl-theme owl-dots-bottom-left" data-nav-dots="true">
            <div class="item">
                <div class="agent text-center">
                    <div class="agent-detail">
                        <div class="agent-avatar avatar avatar-xllll bank-box">
                          <img src="{{asset('public/front/images/sbi.png')}}" alt="sbi" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="agent text-center">
                    <div class="agent-detail">
                        <div class="agent-avatar avatar avatar-xllll bank-box">
                          <img src="{{asset('public/front/images/hdfc.png')}}" alt="hdfc" />
                        </div>

                    </div>
                </div>
            </div>
            <div class="item">
                <div class="agent text-center">
                    <div class="agent-detail">
                       <div class="agent-avatar avatar avatar-xllll bank-box">
                          <img src="{{asset('public/front/images/boi.jpg')}}" alt="boi" />
                        </div>        
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="agent text-center">
                    <div class="agent-detail">
                        <div class="agent-avatar avatar avatar-xllll bank-box">
                          <img class="img-fluid" src="{{asset('public/front/images/axis.png')}}" alt="axis">
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="agent text-center">
                    <div class="agent-detail">
                        <div class="agent-avatar avatar avatar-xllll bank-box">
                          <img class="img-fluid" src="{{asset('public/front/images/icici.png')}}" alt="icici">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--=================================
Meet our agent -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.1.1/pdfobject.min.js"></script>
<script>
PDFObject.embed('{{url("public/images/floor_plan/".$property->floor_plan)}}', "#load_iframe")
</script>

 <script type="text/javascript">
  
    // Function to reveal lightbox and adding YouTube autoplay
    function revealVideo(div,video_id) 
	{
		var video = document.getElementById(video_id).src;
		var index = video.indexOf("?autoplay");
		
		if(index > 0)
         video = video.replace('?autoplay=0','?autoplay=1');
		else
			video = video+'?autoplay=1'; // adding autoplay to the URL
		
		document.getElementById(video_id).src = video;
		document.getElementById(div).style.display = 'block';
    }
    
    // Hiding the lightbox and removing YouTube autoplay
    function hideVideo(div,video_id) {
      var video = document.getElementById(video_id).src;
      var cleaned = video.replace('?autoplay=1','?autoplay=0'); // removing autoplay form url
      document.getElementById(video_id).src = cleaned;
      document.getElementById(div).style.display = 'none';
    }
      </script>
@endsection
@section('script')
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
<script>
    var flag =  false;
    var verifyCallback = function(response) {
        $("#recaptcha_msg").text("");
        flag  = true;
    };
    var widgetId1;
    var widgetId2;
    // var onloadCallback = function() {
    // // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
    // // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
    // grecaptcha.render('recaptcha', {
    //     'sitekey' : '6Ldu6c8ZAAAAAIbXoD48KNPQFI6jVkZjzAMuUPp3',
    //     'callback' : verifyCallback,
    //     'theme' : 'light'
    // });
    // };
    

$(document).ready(function () {
    $("#download_pdf").click(function (e) { 
        var floor_plan = '{{url("public/images/floor_plan/".$property["floor_plan"])}}';
        var link = document.createElement('a');
        link.href = floor_plan;
        link.download = 'floor-Plan.pdf';
        link.dispatchEvent(new MouseEvent('click'));
        
    });
$("#contact_btn").click(function (e) { 
    
  if(flag)
  {
        $("#recaptcha_msg").text("");
        $.ajax({
        type: "post",
        url: '{{url("send-mail")}}',
        data:$("#contact-form").serialize(),
            success: function (response) {
                $('.success-msg').removeClass("d-none");
                $(".success-msg").text("Send Mail Successfully...");
                $(".err_msg").text("");

            },
            error: function (param) { 
                $.each(param.responseJSON.errors , (k,v)=>{
                    $(`#c_${k}`).text(v[0]);
                });
            },
        });
  }
  else
  {
      $("#recaptcha_msg").text("Please checked Recaptcha");
      return false;
  }
});


  }); 
  $('.owl-carousel12').owlCarousel({
        
          items:3,
          loop:true,
          margin:10,
          nav:true,
          autoplay:true,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:3
              },
              1000:{
                  items:4
              }
          }
      });

</script>

 <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v8.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="105584477968961"
        theme_color="#f64546">
      </div> 
@endsection

<?php
function fetchSubStr( $str, $start, $end, &$offsetI=0 )
{
	$pos1 = strpos( $str, $start );
	if( $pos1 !== FALSE )
	{
		$pos1 = $pos1 + strlen( $start );

		$pos2 = FALSE;
		if( !empty( $end ) )	
		{
			$pos2 = strpos( $str, $end, $pos1 );
		}
		
		if( $pos2 !== FALSE )
		{
			$offsetI = $pos2;
			return substr( $str, $pos1, ( $pos2 - $pos1 ) );
		}
		else
		{
			$offsetI = $pos1;
			return substr( $str, $pos1 );
		}
	}
}
?>









