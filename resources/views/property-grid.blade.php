@extends('layouts.front')
@section('content')

<!--=================================
breadcrumb -->
<div class="bg-light">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ol class="breadcrumb mb-0">
          <li class="breadcrumb-item"><a href="index.html"> <i class="fas fa-home"></i> </a></li>
          <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Property grid </span></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!--=================================
breadcrumb -->

<!--=================================
Listing – grid view -->
<section class="space-ptb">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="section-title mb-3 mb-lg-4">
          <!-- <h2><span class="text-primary">156</span> Results</h2> -->
        </div>
      </div>
      <div class="col-md-6">
        <div class="property-filter-tag">
          <ul class="list-unstyled">
            {{-- <li><a href="#">Apartment <i class="fas fa-times-circle"></i> </a></li> --}}
            {{-- <li><a class="filter-clear" href="#">Reset Search <i class="fas fa-redo-alt"></i> </a></li> --}}
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 mb-5 mb-lg-0 d-none">
        <div class="sidebar">
          <div class="widget">
            <div class="widget-title widget-collapse">
              <h6>Advanced filter</h6>
              <a class="ml-auto" data-toggle="collapse" href="#filter-property" role="button" aria-expanded="false" aria-controls="filter-property"> <i class="fas fa-chevron-down"></i> </a>
            </div>
            <div class="collapse show" id="filter-property">
              <form class="mt-3">
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select">
                    <option>All Type</option>
                    <option>Villa</option>
                    <option>Apartment Building</option>
                    <option>Commercial</option>
                    <option>Office</option>
                    <option>Residential</option>
                    <option>Shop</option>
                    <option>Apartment</option>
                  </select>
                </div>
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select">
                    <option>For Rent</option>
                    <option>For Sale</option>
                  </select>
                </div>
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select">
                    <option>Distance from location</option>
                    <option>Within 1 mile</option>
                    <option>Within 3 miles</option>
                    <option>Within 5 miles</option>
                    <option>Within 10 miles</option>
                    <option>Within 15 miles</option>
                    <option>Within 30 miles</option>
                  </select>
                </div>
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select">
                    <option>Bedrooms</option>
                    <option>01</option>
                    <option>02</option>
                    <option>03</option>
                  </select>
                </div>
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select">
                    <option>Sort by</option>
                    <option>Most popular</option>
                    <option>Highest price</option>
                    <option>Lowest price</option>
                    <option>Most reduced</option>
                  </select>
                </div>
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select">
                    <option>Select Floor</option>
                    <option>01</option>
                    <option>02</option>
                    <option>03</option>
                  </select>
                </div>
                <div class="input-group mb-2">
                  <input class="form-control" placeholder="Type (sq ft)">
                </div>
                <div class="input-group mb-2">
                  <input class="form-control" placeholder="Type (sq ft)">
                </div>
                 <div class="form-group property-price-slider mt-3">
                  <label>Select Price Range</label>
                  <input type="text" id="property-price-slider" name="example_name" value="" />
                </div>
                <div class="input-group mb-2">
                  <button class="btn btn-primary btn-block align-items-center" type="submit"><i class="fas fa-filter mr-1"></i><span>Filter</span></button>
                </div>
              </form>
            </div>
          </div>
          <div class="widget">
            <div class="widget-title widget-collapse">
              <h6>Status of property</h6>
              <a class="ml-auto" data-toggle="collapse" href="#status-property" role="button" aria-expanded="false" aria-controls="status-property"> <i class="fas fa-chevron-down"></i> </a>
            </div>
            <div class="collapse show" id="status-property">
              <ul class="list-unstyled mb-0 pt-3">
                <li><a href="#">For rent<span class="ml-auto">(500)</span></a></li>
                <li><a href="#">For Sale<span class="ml-auto">(1200)</span></a></li>
              </ul>
            </div>
          </div>
          <div class="widget">
            <div class="widget-title widget-collapse">
              <h6>Type of property</h6>
              <a class="ml-auto" data-toggle="collapse" href="#type-property" role="button" aria-expanded="false" aria-controls="type-property"> <i class="fas fa-chevron-down"></i> </a>
            </div>
            <div class="collapse show" id="type-property">
              <ul class="list-unstyled mb-0 pt-3">
                <li><a href="#">Residential<span class="ml-auto">(12)</span></a></li>
                <li><a href="#">Commercial<span class="ml-auto">(45)</span></a></li>
                <li><a href="#">Industrial<span class="ml-auto">(23)</span></a></li>
                <li><a href="#">Apartment<span class="ml-auto">(05)</span></a></li>
                <li><a href="#">Building code<span class="ml-auto">(10)</span></a></li>
                <li><a href="#">Communal land<span class="ml-auto">(47)</span></a></li>
                <li><a href="#">Insurability<span class="ml-auto">(32)</span></a></li>
              </ul>
            </div>
          </div>
          <div class="widget">
            <div class="widget-title">
              <h6>Mortgage calculator</h6>
            </div>
            <form>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
                </div>
                <input type="text" class="form-control" placeholder="Total Amount">
              </div>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
                </div>
                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Down Payment">
              </div>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fas fa-percent"></i></div>
                </div>
                <input type="text" class="form-control" placeholder="Interest Rate">
              </div>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
                <input type="text" class="form-control" placeholder="Loan Term (Years)">
              </div>
              <div class="input-group mb-3 select-border">
                <select class="form-control basic-select">
                  <option>Monthly</option>
                  <option>Weekly</option>
                  <option>Yearly</option>
                </select>
              </div>
              <a class="btn btn-primary btn-block" href="#">Calculate</a>
            </form>
          </div>
          <div class="widget">
            <div class="widget-title">
              <h6>Recently listed properties</h6>
            </div>
            <div class="recent-list-item">
              <img class="img-fluid" src="images/property/list/01.jpg" alt="">
              <div class="recent-list-item-info">
                <a class="address mb-2" href="property-detail.html">Awesome family home</a>
                <span class="text-primary">₹1,456,233 </span>
              </div>
            </div>
            <div class="recent-list-item">
              <img class="img-fluid" src="images/property/list/02.jpg" alt="">
              <div class="recent-list-item-info">
                <a class="address mb-2" href="property-detail.html">Contemporary apartment</a>
                <span class="text-primary">₹2,496,454 </span>
              </div>
            </div>
            <div class="recent-list-item">
              <img class="img-fluid" src="images/property/list/03.jpg" alt="">
              <div class="recent-list-item-info">
                <a class="address mb-2" href="property-detail.html">Family home for sale</a>
                <span class="text-primary">₹4,662,457 </span>
              </div>
            </div>
            <div class="recent-list-item">
              <img class="img-fluid" src="images/property/list/04.jpg" alt="">
              <div class="recent-list-item-info">
                <a class="address mb-2" href="property-detail.html">184 lexington avenue</a>
                <span class="text-primary">₹2,456,452 </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-9">
        <div class="property-filter d-sm-flex">
         
          <ul class="property-view-list list-unstyled d-flex mb-0">
         
            <li><a href="index-half-map.html"><i class="fas fa-map-marker-alt fa-lg"></i></a></li>
              <li><a class="property-list-icon" href="{{url('property-list')}}">
              <span></span>
              <span></span>
              <span></span>
            </a></li>
            <li><a class="property-grid-icon active" href="{{url('property-grid')}}">
              <span></span>
              <span></span>
              <span></span>
            </a></li>
          </ul>
        </div>
        <div class="row mt-4">
        @foreach ($property as $item)
          @php  $labelAr = explode(",",$item['label']); @endphp
          <div class="col-sm-6">
            <div class="property-item">
              <div class="property-image bg-overlay-gradient-04">
                <?php 
                  if( isset( $item->images ) && !empty( $item->images ) )
                      echo '<img class="icon" src="'.asset("storage/app/".$item->images['image']).'">';
                  else
                      echo '<img class="icon" src="'.asset("public/images/no-image.png").'">';
                  ?> 
                <div class="property-lable">
                 
                    @foreach ($labelAr as $ar)
                    <span class="badge badge-md badge-primary">{{$ar}}</span>
                    @endforeach
                  
                  {{-- <span class="badge badge-md badge-info">Sale </span> --}}
                </div>
                <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>
                {{-- <div class="property-agent">
                  <div class="property-agent-image">
                    <img class="img-fluid" src="{{asset('public/images/property/'.$image)}}" alt="">
                  </div>
                  <div class="property-agent-info">
                    <a class="property-agent-name" href="#">Alice Williams</a>
                    <span class="d-block">Company Agent</span>
                    <ul class="property-agent-contact list-unstyled">
                      <li><a href="#"><i class="fas fa-mobile-alt"></i> </a></li>
                      <li><a href="#"><i class="fas fa-envelope"></i> </a></li>
                    </ul>
                  </div>
                </div> --}}
                <div class="property-agent-popup d-none">
                  {{-- <a href="#"><i class="fas fa-camera"></i>{{(!empty($item->image_arr)) ? count($item->image_arr) : 0}}</a> --}}
                </div>
              </div>
              <div class="property-details">
                <div class="property-details-inner">
                  <h5 class="property-title"><a href="{{url('property-detail/'.$item['id'])}}">{{$item['name']}} </a></h5>
                  <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i>{{$item['address']}}</span>
                  
                  <div class="property-price">₹{{$item['amount']}}
                  <ul class="property-info list-unstyled d-flex">
                    <li class="flex-fill property-bed"><i class="fas fa-bed"></i>Bed <span>{{$item['bed']}}</span></li>
                    <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span>{{$item['bath']}}</span></li>
                    <li class="flex-fill property-m-sqft"><i class="far fa-square"></i>Sqft<span>{{$item['sqft']}}</span></li>
                  </ul>
                </div> 
              </div>
              <div class="property-btn">
                  <a class="property-link" href="{{url('property-detail/'.$item['id'])}}">See Details</a>
                  <ul class="property-listing-actions list-unstyled mb-0">
                    <li class="property-compare"><a data-toggle="tooltip" data-placement="top" title="Compare" href="#"><i class="fas fa-exchange-alt"></i></a></li>
                    <li class="property-favourites"><a data-toggle="tooltip" data-placement="top" title="Favourite" href="#"><i class="far fa-heart"></i></a></li>
                  </ul>
                </div>
            </div>
          </div>
          </div>
          @endforeach
        </div>
        <div class="row">
          <div class="col-12">
               {{$property->links()}}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Listing – grid view -->
<!--=================================
newsletter -->

@endsection

@section('script')
<!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v8.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
    
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="105584477968961"
        theme_color="#f64546">
      </div>

@endsection