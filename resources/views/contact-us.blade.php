@extends('layouts.front')
@section('content')
    

<!--=================================
breadcrumb -->
<div class="bg-light">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ol class="breadcrumb mb-0">
          <li class="breadcrumb-item"><a href="{{url('/')}}"> <i class="fas fa-home"></i></a>
          </li>  
            <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Contact us </span></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--=================================
  breadcrumb -->
  
  <!--=================================
  Contact -->
  <section class="space-ptb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="section-title">
            <h2>Contact Us</h2>
          </div>
        </div>
      </div>
      <div class="row align-items-center">
        <div class="col-lg-5">
          <div class="contact-address bg-light p-4">
            <div class="d-flex mb-3">
              <div class="contact-address-icon">
                <i class="flaticon-map text-primary font-xlll"></i>
              </div>
              <div class="ml-3 contact-desc">
                <h6>Branch Office:</h6>
                <p>No.21/1, 21st street,4th main road, nanganallur, Chennai - 600 061.</p>

                <div class="contact-desc">
                  <h6>Head Office:</h6>
                  <p>No:19/9, Thambiah Reddy Road, Westmambalam, Chennai - 600033</p>
                </div>
              </div>

              
            </div>
            <div class="d-flex mb-3">
              <div class="contact-address-icon">
                <i class="flaticon-email text-primary font-xlll"></i>
              </div>
              <div class="ml-3 contact-desc">
                <h6>Email</h6>
                <p class="contact-us-link"><a href="mailto:srisuprabhathambuilders@gmail.com">srisuprabhathambuilders@gmail.com</a> </p>
              </div>
            </div>
            <div class="d-flex mb-3">
              <div class="contact-address-icon">
                <i class="flaticon-call text-primary font-xlll"></i>
              </div>
              <div class="ml-3 contact-desc">
                <h6>Phone Number</h6>
                <p class="contact-us-link"><a href="tel:+919962482979">+919962482979</a></p>
                <p class="contact-us-link"><a href="tel:044-22246564"> 044-22246564 / 42648288
                </a></p>
              </div>
            </div>
            <div class="d-flex mb-3">
              <div class="contact-address-icon">
                <i class="flaticon-fax text-primary font-xlll"></i>
              </div>
              <div class="ml-3 contact-desc">
                <h6>Fax</h6>
                <p class="contact-us-link"><a href="tel:044 - 2474 0476">044 - 2474 0476</a></p>
              </div>
            </div>
            <div class="social-icon-02">
              <div class="d-flex align-items-center">
                <h6 class="mr-3">Social:</h6>
                <ul class="list-unstyled mb-0 list-inline">
                  <li><a href="https://www.facebook.com/srisuprabhathambuilders/?ref=settings"> <i class="fab fa-facebook-f"></i> </a></li>
                  <li><a href="https://instagram.com/sri_suprabhatham_builder"> <i class="fab fa-instagram"></i> </a></li>
                  <li><a href="https://twitter.com/srisuprabhatham"> <i class="fab fa-twitter"></i> </a></li>
                  <!--<li><a href="#"> <i class="fab fa-linkedin"></i> </a></li>-->
                  <!--<li><a href="#"> <i class="fab fa-pinterest"></i> </a></li>-->
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-7 mt-4 mt-lg-0">
          <div class="contact-form">
            <h4 class="mb-4">Need assistance? Please complete the contact form</h4>
            @if(Session::has('message')) <div class="alert alert-success"><strong>{{ Session::get('message') }}</strong></div>@endif
            <form action="{{url('contact')}}" method="post">
              @csrf
              <div class="form-row">
                <div class="form-group col-md-6">
                  <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="Your name">
                  @error('name')
                  <em class="text-danger">{{$message}}</em>    
                  @enderror
                </div>
               
                <div class="form-group col-md-6">
                  <input type="email" class="form-control" id="inputEmail4" value="{{old('email')}}"  name="email" placeholder="Your email">
                  @error('email')
                  <em class="text-danger">{{$message}}</em>    
                  @enderror
                </div>
               
                <div class="form-group col-md-6">
                  <input type="text" class="form-control" id="phone" name="phone" value="{{old('phone')}}" placeholder="Your phone">
                  @error('phone')
                  <em class="text-danger">{{$message}}</em>    
                  @enderror
                </div>
               
                <div class="form-group col-md-6">
                  <input type="text" class="form-control" id="subject" name="subject" value="{{old('subject')}}" placeholder="Subject">
                  @error('subject')
                  <em class="text-danger">{{$message}}</em>    
                  @enderror
                </div>
              
                <div class="form-group col-md-12">
                  <textarea class="form-control" rows="4" placeholder="Your message" name="message">{{old('message')}}</textarea>
                  @error('message')
                  <em class="text-danger">{{$message}}</em>    
                  @enderror
                </div>
              
                <div class="form-group col-md-12">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label pr-5" for="customCheck1">I consent to having this website store my submitted information so they can respond to my inquiry.</label>
                  </div>
                </div>
                <div id="recaptcha_contact" class="captcha-contact col-md-12 g-recaptcha"></div> 
                <em class="text-danger" id="recaptcha_msg"></em>    
                
                <div class="col-md-12 mt-3">
                  <button type="submit" class="btn btn-primary mt-3"  id="contact_btn">Send message</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
  
      <div class="row mt-1 mt-md-2 d-none">
        <div class="col-12">
          <h4 class="mb-4 my-4 my-sm-5 addtional-info">Additional contact Info</h4>
        </div>
        <div class="col-md-4">
          <div class="d-flex">
            <i class="flaticon-time-call font-xlll text-primary"></i>
            <div class="ml-4 office-desc">
              <h5>Estate agency offices</h5>
              <p>Our Estate Agency offices can help with you buying or selling a home.</p>
              <a href="#">Click to contact an estate agency branch</a>
            </div>
          </div>
        </div>
        <div class="col-md-4 mt-4 mt-md-0">
          <div class="d-flex">
            <i class="flaticon-email-1 font-xlll text-primary"></i>
            <div class="ml-4 office-desc">
              <h5>Lettings offices</h5>
              <p>Our Lettings offices can assist with you letting your home, protection and moving home.</p>
              <a href="#">Click to contact a lettings branch</a>
            </div>
          </div>
        </div>
        <div class="col-md-4 mt-4 mt-md-0">
          <div class="d-flex">
            <i class="flaticon-group font-xlll text-primary"></i>
            <div class="ml-4 office-desc">
              <h5>Chat to us online</h5>
              <p class="mb-0">Chat to us online if you have a question about using our Mortgage calculator.</p>
              <a class="btn btn-primary btn-sm mt-3" href="#"> Start web chat</a>
            </div>
          </div>
        </div>
      </div>
  
      <div class="mt-5">
        <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3886.9547340416298!2d80.22116521413581!3d13.038553316935033!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a5266fc8a7b6ffd%3A0x61db27359241dfc7!2sSri%20Suprabhatham%20Builder!5e0!3m2!1sen!2sin!4v1600677871669!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->

        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3887.817623314174!2d80.18625541413533!3d12.983513818131255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a525dfdfcba3673%3A0xa3bc3794aa478d19!2s4th%20Main%20Rd%2C%20Nanganallur%2C%20Chennai%2C%20Tamil%20Nadu!5e0!3m2!1sen!2sin!4v1601888996440!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      </div>
  
    </div>
  </section>
@endsection
@section('script')
{{-- <script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.sitekey') }}"></script> --}}
{{-- <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script> --}}
<script>
        //  grecaptcha.ready(function() {
        //      grecaptcha.execute('{{ config('services.recaptcha.sitekey') }}', {action: 'contact'}).then(function(token) {
        //         if (token) {
        //           document.getElementById('recaptcha').value = token;
        //         }
        //      });
        //  });
  
  var flag =  false;
$("#contact_btn").click(function (e) { 

  if(flag)
  {
    $("#recaptcha_msg").text("");
  }
  else
  {
    $("#recaptcha_msg").text("Please checked Recaptcha");
      return false;
  }
});

var verifyCallback = function(response) {
     flag  = true;
     $("#recaptcha_msg").text("");
  };
//  var widgetId1;
//  var widgetId2;
//  var onloadCallback = function() {
//    // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
//    // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
//   grecaptcha.render('recaptcha_contact', {
//      'sitekey' : '6Ldu6c8ZAAAAAIbXoD48KNPQFI6jVkZjzAMuUPp3',
//      'callback' : verifyCallback,
//      'theme' : 'light'
//    });
//  };
</script>


<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v9.0'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="105584477968961"
theme_color="#f64546"
logged_in_greeting="You can dream, create, design, and build the most wonderful place in the world."
logged_out_greeting="You can dream, create, design, and build the most wonderful place in the world.">
</div>
@endsection