@extends('layouts.front')
@section('content')
    
<!--=================================
breadcrumb -->
<div class="bg-light">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="index.html"> <i class="fas fa-home"></i> </a></li>
            
            <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> About us </span></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--=================================
  breadcrumb -->
  
  <!-- Begin::about -->
  <section class="my-5"> 
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="ssb-about">
            <h1>SRI <br/>SUPRABHATHAM <br/><span class="builder"> BUILDER</span></h1>
          </div>
        </div>
        <div class="col-md-4">
          <p class="sri-about">
            M/s. Sri Suprabhatham builder was incorporated in 2002. The firm is engaged in the business of developing and construction flats over the last two decades. The firm also operates as flats promoters & developers.Mr.R.Ravichandran have been into this business in the name of Sri Suprabhatham builder and managed by his son & himself. An M/S. Sri Suprabhatham builder is being managed by a group of professionals who are committed to providing qualitative and value-based apartments to satisfy the needs of the consumers. The rich collective experience of over 40 man-years in the industry has provided us the imputes to render efficient and cost-effective projects so as to cater to the demands of lower and upper-middle-class families.
          </p>
        </div>
        <div class="col-md-4">
          <p class="sri-about">
            Our teams, our people are our greatest strengths .over two decades of professional experience has given us what are today our core strengths viz. Strong project Management Skills. Good academic background, and strong working processes and methodologies. The promoters have had a good consistent track record and the functioning of the company has consistently been impressive during the last few years a detailed report of the completed projects is described in annexure I hereto
          </p>
        </div>
      </div>
    </div>
  </section>
  <!-- end::about -->
  
  <!--========  testimonial -->
  <section class="testimonial-main bg-holder" style="background-image: url({{asset('public/front/images/Ravichhandran.jpg')}});">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div>
            <div class="">
              <div class="testimonial">
                <div class="testimonial-content">
                  <p><i class="quotes">"</i>We value your Money and respect your time. Standing with us in good stead in these endeavors is our unwavering commitment to delivering nothing less than the very best, in terms of both quality and value. </p>
                </div>
                <div class="testimonial-name">
                  <h6 class="text-primary mb-1">MR.RAVICHANDRAN,</h6>
                  <span>Founder & Director.,</span>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--======== testimonial -->
  
  <div class="container my-5">
    <p class="sri-about">
      Completing over 35 projects in the last twenty years gave us the strength to carry on and dream bigger. Knowing that you will be always satisfied with what we offer, gives us an even longer ladder to reach the sky. Our ongoing projects are more of a modernized and exceedingly meeting the international standards. You may feel like being accommodated overseas with experience you are about to sense. All our projects meet the quality standards and only the best of the best materials are used to construct your homes. For this statement, Mr. R. Ravichandran is willing to personally testify to it. And they have made it a point that nothing should ever be compromised with the quality of the materials used. They have been partners since the beginning and they tend to manage everything running by themselves.
    </p>
  </div>
  
  <!--================================= testimonial -->
  <section class="testimonial-main bg-holder" style="background-image: url({{asset('public/front/images/Kishore.jpg')}});">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div>
            <div class="">
              <div class="testimonial">
                <div class="testimonial-content">
                  <p><i class="quotes">"</i>Our Concepts and plans are crafted by renowned designers and architects, to provide everyone a dream home that's stylish with a modern layout to give our clients a luxurious Home for the best affordable price.</p>
                </div>
                <div class="testimonial-name">
                  <h6 class="text-primary mb-1">MR.KISHORE RAVICHANDRAN,</h6>
                  <span>Managing Director.,</span>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================================= testimonial -->
  <div class="container my-5">
    <p class="sri-about">
      I grew up watching my dad spend his life trying to satisfy his customers and people with the best he can in this business. Being from a small background he could build this business from nothing to a well-established group of companies. That’s when I realized when he could bring a huge success from very little, why can’t an MBA from London hike the business achieve a bigger triumph. I am looking at a different set of target customers. Mostly NRIs and foreigners who would want their homes to be of an international standard not only for a lifestyle purpose but also for the value for the amount of money they spend. I have learned to adapt to the situation & environment and that I have to be the best in what I do to be the best in the industry. Being able to create an upgraded level of lifestyle matching the latest trend for your home is my ultimate goal. Learning the business from dad is set off to open a group of Kishore Homes. Though I’m still learning every day with my father’s blessings and your good wishes.
    </p>
  </div>
  
  
  
  <!--=================================
  Meet our agent -->
  <!--==== Meet our agent -->
  <section class="mt-5 mb-5 cust-mar-bottom">
      <div class="section-title text-center">
            <h2>Testimonial</h2>
            <p>Our Most Valuable Testimonial Statements</p>
      </div>
        
      <div class="container">
        <div class="owl-carousel-testimonial owl-theme owl-dots-bottom-left" data-nav-dots="true">
            <div class="item">
              <div class="row">
                 <div class="col-md-4">
                   <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                        <img class="rounded-circle" src="{{asset('public/front/images/SRINATH  (SCRTEL ELECTRICS LTD  U.K).png')}}" alt="">
                    </div>
                 </div>
                 <div class="col-md-8">
                    <div class="agent-info">
                      <i class="fas fa-quote-left"></i>
                        <h6 class="mb-0"><a href="agent-detail.html"></a>Srinath</h6>
                        <span class="text-primary font-sm">SCRTEL ELECTRICS LTD  U.K</span>
                        <p class="mt-3 mb-0">Prior to choosing SSB, we walked through countless new construction homes. After seeing a Ravichandran Home, it was clear that the quality and attention to detail were next level. It was great working with Kishore and his team throughout the building process. They were easy to get in touch with and quick to respond to any questions that came up. The project management document that they provided was very transparent, made the selection process easy, and aided in communication throughout the entirety of the build. We would certainly recommend Sri Suprabhatham Builders and would be happy to work with them again in the future.
                        </p>
                      </div>
                 </div>
              </div>
            </div>
            <div class="item">
                <!-- <div class="agent text-center">
                    <div class="agent-detail">
                        <div class="agent-avatar avatar avatar-xllll">
                          <img class="img-fluid rounded-circle" src="images/profile-1.jpg" alt="">
                        </div>
                        <div class="agent-info">
                          <h6 class="mb-0"> <a href="agent-detail.html">Alice Williams </a></h6>
                          <span class="text-primary font-sm">Founder & CEO </span>
                          <p class="mt-3 mb-0">The first thing to remember about success is that it is a process – nothing more, nothing less.</p>
                        </div>
                    </div>
                </div> -->

                <div class="row">
                   <div class="col-md-4">
                     <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                          <img class="rounded-circle" src="{{asset('public/front/images/AMIRTHAVALLI ( ADVOCATE).png')}}" alt="" />
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="agent-info">
                        <i class="fas fa-quote-left"></i>
                          <h6 class="mb-0"> <a href="agent-detail.html">Amirthavalli</a></h6>
                          <span class="text-primary font-sm">Advocate </span>
                          <p class="mt-3 mb-0">
                            We had the privilege and honor of being the first client of Sri Suprabhatham Builder. With Mr. Ravichandran Dedication, Sincerity, and Honesty we understand that we made the right choice for buying our apartment from them. We wish them all the best and we are sure they will go a long way.
                          </p>
                        </div>
                   </div>
                </div>
            </div>

            <div class="item">
                <div class="row">
                   <div class="col-md-4">
                     <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                          <img class="rounded-circle" src="{{asset('public/front/images/BALASUBRAMANIAN (BHARATH ELECTRICAL LTD).png')}}" alt="">
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="agent-info">
                        <i class="fas fa-quote-left"></i>
                          <h6 class="mb-0"> <a href="agent-detail.html">Balasubramanian</a></h6>
                          <span class="text-primary font-sm">Bharath Electrical PVT LTD. </span>
                          <p class="mt-3 mb-0">We are very much pleased with the Management of SSB for their Transparency in Documentation & Legal aspects. They have good patience in hearing the Client’s point of view and lend them valuable Advice & Ideas for the project. The execution of the project is Time-Oriented. We wish them huge success for their future projects and i will definitely refer to my other family members and colleagues as well.
                          </p>
                        </div>
                   </div>
                </div>
            </div>

            <div class="item">
                <div class="row">
                   <div class="col-md-4">
                     <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                          <img class="rounded-circle" src="{{asset('public/front/images/Dr. ISHWARYA.png')}}" alt="">
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="agent-info">
                        <i class="fas fa-quote-left"></i>
                          <h6 class="mb-0"> <a href="agent-detail.html">Dr. Ishwarya </a></h6>
                          <span class="text-primary font-sm">Doctor</span>
                          <p class="mt-3 mb-0">If you are looking for a seasoned and skilled builder focused on honesty, integrity, fantastic communication, extreme attention to detail, and quality contractors... call Kishore Kannan! So impressed how they have built our home from idea in our heads to reality. Every day we are more impressed.
                          </p>
                        </div>
                   </div>
                </div>
            </div>

            <div class="item">
                <div class="row">
                   <div class="col-md-4">
                     <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                          <img class="rounded-circle" src="{{asset('public/front/images/G.MURUGAN (GOVCHE INDIA PVT LTD).png')}}" alt="">
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="agent-info">
                        <i class="fas fa-quote-left"></i>
                          <h6 class="mb-0"><a href="agent-detail.html">G.Murugan </a></h6>
                          <span class="text-primary font-sm">GOVCHE INDIA PVT LTD </span>
                          <p class="mt-3 mb-0">A fantastic builder with a reputation for being transparent and customer friendly. Approachable 24X7, they have a dedicated team for every department to ensure that quality is uncompromised. For SSB, it’s not merely a contract or sale but a relation, in which they ensure that they address any issue we may face at any point of time. I would definitely recommend SSB to all property buyers and vouch for the fact that SSB is a builder you can trust.
                          </p>
                        </div>
                   </div>
                </div>
            </div>

            <div class="item">
                <div class="row">
                   <div class="col-md-4">
                     <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                          <img class="rounded-circle" src="{{asset('public/front/images/SRIKANTHGOPALAN (SCRTEL ELECTRICS  DIRECTORATE.png')}}" alt="">
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="agent-info">
                        <i class="fas fa-quote-left"></i>
                          <h6 class="mb-0"><a href="agent-detail.html">Srikanthgopalan </a></h6>
                          <span class="text-primary font-sm">SCRTEL ELECTRICS  DIRECTORATE </span>
                          <p class="mt-3 mb-0">Prior to choosing SSB, we walked through countless new construction homes. After seeing a Ravichandran Home, it was clear that the quality and attention to detail were next level. It was great working with Kishore and his team throughout the building process. They were easy to get in touch with and quick to respond to any questions that came up. The project management document that they provided was very transparent, made the selection process easy, and aided in communication throughout the entirety of the build. We would certainly recommend Sri Suprabhatham Builders and would be happy to work with them again in the future.
                          </p>
                        </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
  </section>
  <!--=================================
  Meet our agent -->
  
@endsection

@section('script')
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v9.0'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="105584477968961"
theme_color="#f64546"
logged_in_greeting="You can dream, create, design, and build the most wonderful place in the world."
logged_out_greeting="You can dream, create, design, and build the most wonderful place in the world.">
</div>
@endsection