@extends('layouts.admin')
@section('content')
@section('css')
    <style>
        select {
            width: 100%;
        }
    </style>
      <script type="text/javascript" src="{{asset('public/js/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript">
    	$(document).ready(function(e) {
    	    var description = document.getElementById('sp_description');
    	   	CKEDITOR.replace( description,
    	    {
    	        filebrowserBrowseUrl : 'kcfinder/browse.php',
    	        filebrowserImageBrowseUrl : 'kcfinder/browse.php?type=Images',
    	        filebrowserUploadUrl : 'kcfinder/upload.php',
    	        filebrowserImageUploadUrl : 'kcfinder/upload.php?type=Images'
    			
    		
    	    });
    	     var description = document.getElementById('edit_description');
    	   	CKEDITOR.replace( description,
    	    {
    	        filebrowserBrowseUrl : 'kcfinder/browse.php',
    	        filebrowserImageBrowseUrl : 'kcfinder/browse.php?type=Images',
    	        filebrowserUploadUrl : 'kcfinder/upload.php',
    	        filebrowserImageUploadUrl : 'kcfinder/upload.php?type=Images'
    			
    		
    	    });
        });
    </script>
@endsection

<?php
    if( !empty( session()->getOldInput() ) )
    {
         $property_specification = [];
         $property_flat = [];
         $spe_id = old('spe_id');
         $name = old('name');
         $description = old('description');
         $property_specification = [];
         $property_flat = [];
         if(!empty($name))
         {
             for($i=0;$i<count($name);$i++){
                 $property_specification[$i]['name'] = $name[$i];
                 $property_specification[$i]['specification_id'] = $spe_id[$i];
                 $property_specification[$i]['description'] = $description[$i];
             }
         }
         $floor = old('floor');
         $sqft = old('sqft');
         $bhk = old('bhk');
         $status = old('status');
         if(!empty($floor))
         {
             for($i=0;$i<count($floor);$i++){
                 $property_flat[$i]['floor'] = $floor[$i];
                 $property_flat[$i]['bhk'] = $bhk[$i];
                 $property_flat[$i]['status'] = $status[$i];
                 $property_flat[$i]['sqft'] = $sqft[$i];
             }
         }
    }

?>
<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4 mb-3 border-bottom">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <h3 class="page-title">Property</h3>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Modal -->
    <div class="modal fade theam-modal" id="edit_specification" tabindex="-1" role="dialog"
        aria-labelledby="add-ShippingBillLabel" aria-hidden="true">
        <div class="modal-dialog model-martop1" role="document">
            <div class="modal-content model-martop1">
                <div class="modal-header">
                    <h4 class="modal-title text-uppercase" id="add-ShippingBillLabel">Specification Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="row_id" id="hidden_row_id" value="" />
                    <div class="form-group">
                        <label>Specification</label>
                        <select class="form-control" id="edit_sp_name" disabled>
                            <option value="">Choose...</option>
                            @foreach ($Specification as $item)
                                <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" id="edit_description" style="resize: none;"></textarea>
                    </div>
                </div>
                <div class="modal-footer border-top-0">
                    <button type="button" id="edit" name="edit" class="btn btn-success btn-width" value="Edit">
                        Save</button>
                    <button type="button" class="btn btn-danger btn-width" data-dismiss="modal"> Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end Edit Modal -->
    <!-- Modal -->
    <div class="modal fade theam-modal" id="edit_flat" tabindex="-1" role="dialog"
        aria-labelledby="add-ShippingBillLabel" aria-hidden="true">
        <div class="modal-dialog model-martop1" role="document">
            <div class="modal-content model-martop1">
                <div class="modal-header">
                    <h4 class="modal-title text-uppercase" id="add-ShippingBillLabel">Flat Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="row_id" id="hidden_row_id" value="" />
                    <div class="form-group">
                        <label>floor</label>
                        <select class="form-control" id="edit_floor">
                            <option value="">Choose...</option>
                            <option value="GF">GF</option>
                            <option value="I">I</option>
                            <option value="II">II</option>
                            <option value="III">III</option>
                            <option value="IV">IV</option>
                            <option value="V">V</option>
                            <option value="VI">VI</option>
                            <option value="VII">VII</option>
                            <option value="VIII">VIII</option>
                            <option value="IX">IX</option>
                            <option value="X">X</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>SQ Feet</label>
                        <input class="form-control" id="edit_sqft" style="resize: none;" />
                    </div>
                    <div class="form-group">
                        <label>BHk</label>
                        <input class="form-control" id="edit_bhk" style="resize: none;"
                            onkeypress="return isNumber(event)" />
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" id="edit_status">
                            <option value="">Choose...</option>
                            <option value="1">Sold</option>
                            <option value="2">Avalilable</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer border-top-0">
                    <button type="button" id="edit_flat_save" class="btn btn-success btn-width" value="Edit">
                        Save</button>
                    <button type="button" class="btn btn-danger btn-width" data-dismiss="modal"> Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end Edit Modal -->
    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom my-3">
                    <h4 class="m-0 text-uppercase">Edit Property</h4>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                        <div class="row">

                            <div class="col-sm-12">
                                <form method="post" action="{{ url('ssb-admn/property/edit/' . $property->id) }}" enctype="multipart/form-data" id="propertyForm">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Name <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" name="property_name" value="{{ $property->name }}" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                @error('name')
                                                    <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Address <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" name="address" value="{{ $property->address }}" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                @error('address')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>SQ Feet <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" name="property_sqft" value="{{ $property['sqft'] }}" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                @error('property_sqft')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Amount <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" name="amount" onkeypress="return isNumber(event)" value="{{ $property->amount }}" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                @error('amount')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Bed <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" name="bed"
                                                        onkeypress="return isNumber(event)" value="{{$property->bed }}" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                @error('bed')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Bath <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" name="bath" onkeypress="return isNumber(event)" value="{{ $property->bath }}" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                @error('bath')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Country <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <select class="form-control" id="country" name="country" required="required">
                                                    <option>Choose...</option>
                                                    @foreach ($country as $item)
                                                        <option value="{{$item->country_id}}" {{($item->country_id == $property->country) ? "selected" : ""}}>{{$item->country_name}}</option>
                                                    @endforeach
                                                </select>
                                                @error('country')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>State <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <select class="form-control" id="state" name="state" required="required">
                                                    <option value="">Choose...</option>
                                                </select>
                                                @error('state')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>City <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <select class="form-control" id="city" name="city" required="required">
                                                    <option value="">Choose...</option>
                                                </select>
                                                @error('city')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Area <em class="text-danger">*</em></label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="area" value="{{$property->area}}" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                @error('area')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    </div>
                                    <div class="row">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Pincode <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" name="pincode" value="{{$property->pincode}}" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                @error('pincode')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                          <label>Label <em class="text-danger">*</em></label>
                                                <div id="output"></div>
                                                @php
                                                
                                                    $labelArr = explode("," , $property->label);
                                                   
                                                @endphp
                                                <select data-placeholder="Choose Label ..." name="label[]" multiple class="chosen-select" required="required">
													<option value="Up Coming" {{((!empty(old('label')) && in_array("Up Coming",$labelArr)) ?  "selected" : (!empty($labelArr) && in_array("Up Coming",$labelArr))) ? "selected" : ""}}>Up Coming</option>
<option value="Bungalow" {{((!empty(old('label')) && in_array("Bungalow",$labelArr)) ?  "selected" : (!empty($labelArr) && in_array("Bungalow",$labelArr))) ? "selected" : ""}}>Bungalow</option>
<option value="Completed" {{((!empty(old('label')) && in_array("Completed",$labelArr)) ?  "selected" : (!empty($labelArr) && in_array("Completed",$labelArr))) ? "selected" : ""}}>Completed</option>
<option value="Sold Out" {{((!empty(old('label')) && in_array("Sold Out",$labelArr)) ?  "selected" : (!empty($labelArr) && in_array("Sold Out",$labelArr))) ? "selected" : ""}}>Sold Out</option>
<option value="On going" {{((!empty(old('label')) && in_array("On going",$labelArr)) ?  "selected" : (!empty($labelArr) && in_array("On going",$labelArr))) ? "selected" : ""}}>On going</option>

                                                </select>
                                                @error('label')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Image <em class="text-danger">*</em></label>
                                            <div class="old-images">
                                                @if( !isEmptyArr( $property->imageArr ) )
                                                @foreach ( $property->imageArr as $k=>$item)
                                                <div class="img-layout">
                                                    <a href="javascript:void(0);" data-id="{{$item['id']}}" class="img-delete"><i class="fas fa-minus-circle"></i></a>
                                                    <img src="{{ asset('storage/app/'.$item['image'])}}" alt="">
                                                </div>
                                                @endforeach
                                                @endif
                                            </div>
                                            <div id="imageData">
                                                {{-- @if( !isEmptyArr( $property->imageArr ) ) --}}
                                                    {{-- @foreach ( $property->imageArr as $k=>$item) --}}
                                                        {{-- <div class="image">
                                                            <img src="{{ asset('storage/app/'.$item['image'])}}" width="100" height="100" id="artPrevImage_{{$k}}" class="image"style="margin-bottom:0px;padding:3px;" alt="logo" /><br />
                                                        	<input type="file" name="image[]" id="ariImg_{{$k}}" onchange="readURL(this,{{$k}});" style="display: none;" accept="image/jpg,image/png">
                                                            <input type="hidden" value="{{$item['image']}}" name="image_val[]" id="hiddenArtImgLogo" />
                                                            <a onclick="$('#ariImg_{{$k}}').trigger('click');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style="clear:both;" onclick="javascript:clear_image('artPrevImage_{{$k}}')">Clear</a>
                                                        </div> --}}
                                                    {{-- @endforeach
                                                @endif --}}
                                            </div>
                                            <button id="add_image" onclick="return false;" class="btn btn-primary my-3">Add Image<i class="fa fa-plus-circle ml-2" aria-hidden="true"></i></button>
                                            @error('image')
                                            <em class="text-danger">{{ $message }}</em>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label>Floor Plan</label>
                                            <div class="form-group">
                                                <div class="image">
                                                    <img src="{{ asset('public/images/pdf.png') }}"
                                                        width="100" height="100" id="artPrevImage_111" class="image"
                                                        style="margin-bottom:0px;padding:3px;" alt="logo" /><br />
                                                    <input type="file" name="floor_plan" id="ariImg_111"
                                                        onchange="readURL(this,'111','1');" style="display: none;"
                                                           accept=".pdf">
                                                    <input type="hidden" value="" name="cd_logo"
                                                        id="hiddenArtImgLogo" />
                                                    <a
                                                        onclick="$('#ariImg_111').trigger('click');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a
                                                        style="clear:both;"
                                                        onclick="javascript:clear_image('artPrevImage_111' , 'pdf.png')">Clear</a>
                                                </div>
                                                @error('floor_plan')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>


                                    </div>
                                   
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Video</label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="video" value="{{$property->video}}">
                                                </div>
                                                @error('video')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Map</label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <textarea type="text" class="form-control" name="map" >{{ $property->map }}</textarea>
                                                </div>
                                                @error('map')
                                                <em class="text-danger">{{ $message }}</em>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Amenity <span class="text-danger">*</span></label>
                                            <div id="output"></div>
                                            <select data-placeholder="Choose Amenity ..." name="amenity[]" multiple
                                                class="chosen-select">
                                                @php
                                                    $amenityAr = explode("," , $property->amenity);
                                                @endphp
                                                @foreach ($amenity as $item)
                                                    <option value="{{ $item->id }}" {{  (!empty(old('amenity')) && in_array($item->id,old('amenity'))) ? "selected" : (!empty($amenityAr) && in_array($item->id,$amenityAr)) ? "selected" : "" }} >
                                                        {{ $item->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('amenity')
                                            <em class="text-danger">{{ $message }}</em>
                                            @enderror
                                        </div>
                                       
                                    </div>
                                    <div class="card-header border-bottom mx--15 my-3">
                                        <h4 class="m-0 text-uppercase">Specification</h4>
                                    </div>
                                    <div class="form-row mt-5 " id="fetch_product_info">
                                        <div class="col-md-4">
                                            <label>Specification <span class="text-danger">*</span></label>
                                            <div class="form-group">
                                                <select class="form-control" id="sp_name">
                                                    <option value="">Choose...</option>
                                                    @foreach ($Specification as $item)
                                                        <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <span id="err_specification" class="text-danger"></span>
                                        </div>
                                       
                                    </div>
                                    <div class="row">
                                         <div class="col-md-12">
                                            <label>Description</label>
                                            <textarea name="sp_description" type="text" class="form-control" id="sp_description"></textarea>
                                            <span id="err_description" class="text-danger"></span>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="invisible">add</label>
                                            <input type="button" class="btn btn-primary d-block w-100 invoice_add" name="" id="addSpecification" value="Add">
                                        </div>
                                    </div>
                                

                                    <div class="table-responsive mt-5">
                                        <table class="table page-table">
                                            <thead class="back thead-light">
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Specification Name</th>
                                                    <th scope="col" style="width: 50%;">Description</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="product_data">
                                                @php
                                                    $empty = true;
                                                    $totalRes = 0;
                                                @endphp
                                                @if (!empty($property_specification))
                                                    @foreach ($property_specification as $k => $ar)
                                                      @php
                                                           $totalRes++;
                                                            $k += 1;
                                                      @endphp
                                                        <tr id="row_{{$k}}"
                                                            class="new_row">
                                                            <th>{{$k}}</th>
                                                            <td>{{$ar['name']}}
                                                                <input type="hidden" name="name[]"
                                                                    id="spe_name{{$k}}"
                                                                    value="{{$ar['name']}}" />
                                                                <input type="hidden"
                                                                    value="{{$ar['specification_id']}}"
                                                                    id="spe_id{{$k}}"  name="spe_id[]">
                                                            </td>
                                                            <td>{!! $ar['description']!!}<input type="hidden"
                                                                    id="spe_description{{$k}}"
                                                                    name="description[]"
                                                                    value="{{$ar['description']}}" />
                                                            </td>

                                                            <td>
                                                                <span class="d-inline-block back_col view_details"
                                                                    tabindex="0" data-toggle="tooltip"
                                                                    title="Edit Specification"
                                                                    id="{{$k}}">
                                                                    <a class="btn btn-primary p-2" style="color:white;">
                                                                        <!-- <i class="fa fa-pencil-alt" aria-hidden="true"></i> -->
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                    </a>
                                                                </span>
                                                                <span class="d-inline-block back_col remove_details"
                                                                    tabindex="0" title="Delete Specification"
                                                                    id="{{$k}}">
                                                                    <a class="btn btn-danger p-2" style="color:white;">
                                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                                    </a>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    @php
                                                    $empty = false;
                                                    @endphp
                                                @endif

                                                @if ($empty)
                                                    <tr id="empty_table">
                                                        <td id="empty_table_colspan" colspan="4">You haven't added Specification yet.</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-header border-bottom mx--15 py-3">
                                        <h4 class="m-0 text-uppercase">Flats</h4>
                                    </div>
                                    <div class="form-row mt-5 " id="fetch_product_info">

                                        <div class="col-lg-3 col-md-4 form-group">
                                            <label>Floor</label>
                                            <select class="form-control" id="floor">
                                                <option value="">Choose...</option>
                                                <option value="GF">GF</option>
                                                <option value="I">I</option>
                                                <option value="II">II</option>
                                                <option value="III">III</option>
                                                <option value="IV">IV</option>
                                                <option value="V">V</option>
                                                <option value="VI">VI</option>
                                                <option value="VII">VII</option>
                                                <option value="VIII">VIII</option>
                                                <option value="IX">IX</option>
                                                <option value="X">X</option>
                                            </select>
                                            <span id="err_floor" class="text-danger"></span>
                                        </div>
                                        <div class="col-lg-3 col-md-4 form-group">
                                            <label>SQ Feet</label>
                                            <input type="text" class="form-control" id="sqft">
                                            <span id="err_sqft" class="text-danger"></span>
                                        </div>
                                        <div class="col-lg-3 col-md-4 form-group">
                                            <label>BHK</label>
                                            <input type="text" class="form-control" id="bhk"
                                                onkeypress="return isNumber(event)">
                                            <span id="err_bhk" class="text-danger"></span>
                                        </div>
                                        <div class="col-lg-1 col-md-4 col-6 mx-auto form-group">
                                            <label>Status</label>
                                            <div class="form-group">
                                                <select class="form-control" id="status">
                                                    <option value="">Choose...</option>
                                                    <option value="1">Sold</option>
                                                    <option value="2">Available</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-1 col-md-4 col-6 mx-auto form-group">
                                            <label class="invisible">add</label>
                                            <input type="button" class="btn btn-primary d-block w-100 invoice_add"
                                                name="" id="addflat" value="Add">
                                        </div>
                                    </div>
                                    <div class="table-responsive mt-5">
                                        <table class="table page-table">
                                            <thead class="back thead-light">
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Floor</th>
                                                    <th scope="col">SQ Feet</th>
                                                    <th scope="col">BHK</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="flat_data">
                                                @php
                                                $fempty = true;
                                                $totalflatRes = 0;
                                            @endphp
                                            @if (!empty($property_flat))
                                                @foreach ($property_flat as $k => $ar)
                                                  @php
                                                       $totalflatRes++;
                                                         $k += 1;
                                                  @endphp
                                                    <tr id="frow_{{$k}}"
                                                        class="new_row">
                                                        <th>{{$k}}</th>
                                                        <td>{{$ar['floor']}}
                                                            <input type="hidden" name="floor[]"
                                                                id="floor{{$k}}"
                                                                value="{{$ar['floor']}}" />

                                                        </td>
                                                        <td>{{$ar['sqft']}}<input type="hidden"
                                                                id="sqft{{$k}}"
                                                                name="sqft[]"
                                                                value="{{$ar['sqft']}}" />
                                                        </td>
                                                        <td>{{$ar['bhk']}}<input type="hidden"
                                                            id="bhk{{$k}}"
                                                            name="bhk[]"
                                                            value="{{$ar['bhk']}}" />
                                                        </td>
                                                        <td>
                                                            {{($ar['status'] == '1') ? "Sold" : "Available"}}
                                                            <input type="hidden" id="status{{$k}}" name="status[]" value="{{$ar['status']}}" />
                                                        </td>
                                                        <td>
                                                            <span class="d-inline-block back_col view_flat_details" tabindex="0" data-toggle="tooltip" title="Edit Flat" id="{{$k}}">
                                                                <a class="btn btn-primary p-2" style="color:white;">
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                </a>
                                                            </span>
                                                            <span class="d-inline-block back_col remove_flat_details" tabindex="0" title="Delete Flat" id="{{$k}}">
                                                                <a class="btn btn-danger p-2" style="color:white;">
                                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                                </a>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                @php
                                                $fempty = false;
                                                @endphp
                                            @endif

                                            @if ($fempty)
                                                <tr id="empty_table">
                                                    <td id="empty_table_colspan" colspan="4">You haven't added Flat yet.</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col mb-4 d-flex justify-content-end">
                                        <button type="submit" id="save_property" class="bg-success rounded text-white text-center py-2 px-3 d-inline-block border-0" style="box-shadow: inset 0 0 5px rgba(0,0,0,.2);">Save</button>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>
@endsection
@section('script')
<script src='https://harvesthq.github.io/chosen/chosen.jquery.js'></script>
<script src="./script.js"></script>
<script>
     var state = '{{$property->state}}';
     var city =  '{{$property->city}}';
    document.getElementById('output').innerHTML = location.search;
    $(".chosen-select").chosen();
    var base_url = '{{ url('/') }}';

    $(document).ready(function() {
        $("#sp_name").change(function() {
            val = $(this).val();
            $.ajax({
                type: "get",
                url: base_url + "/ssb-admn/specification/getspecification/" + val,
                success: function(response) {
                    CKEDITOR.instances.sp_description.setData( response.description );                    
                }
            });
        });
        var city_id = $("#state_id").children("option:selected").val();
        $.ajax({
            type: "get",
            url: base_url+'/getcity/'+city_id,
            success: function (response) {
                $("#city").empty();
                $.each(response, function (i, v) { 
                    $("#city").append(`<option value='${v.city_id}' ${(v.city_name == city ) ? 'selected' : ''}>${v.city_name}</option>`);
                });
                
            }
        });
        
        var isShowHide = false;
        var count = '{{$totalRes}}';
        $('#addSpecification').click(function() {

            //set error message product/service and client
            var error_sp_name = '',
                sp_description = '',
                sp_name = $('#sp_name').children("option:selected").val();
            if (sp_name == '') {
                error_sp_name = 'Select at least one Specification';
                $('#err_specification').text(error_sp_name);
                $('#sp_name').css('border-color', '#cc0000');
            } else {
                error_sp_name = '';
                $('#err_specification').text(error_sp_name);
                $('#sp_name').css('border-color', '');
            }
            if (CKEDITOR.instances.sp_description.getData() == '') {
                sp_description = 'Enter Description';
                $('#err_description').text(sp_description);
                $('#sp_description').css('border-color', '#cc0000');
            } else {
                sp_description = '';
                $('#err_description').text(sp_description);
                $('#sp_description').css('border-color', '');
            }
            if (error_sp_name != '' || sp_description != '')
                return false;
            else {
                var isHide = (!isShowHide) ? "d-none" : "";
                $("#empty_table").remove();

                var specification_name = $('#sp_name').children("option:selected").text(),
                    specification_val = $('#sp_name').children("option:selected").val(),
                    specification_Desc = CKEDITOR.instances.sp_description.getData();

                count = (parseInt(count) + parseInt(1));
                output = '<tr id="row_' + count + '" class="new_row">';
                output += '<th>' + count + '</th>';
                output += '<td class="left">' + specification_name +
                    ' <input type="hidden" name="name[]" id="spe_name' + count + '"  value="' +
                    specification_name + '" /><input type="hidden" name="spe_id[]" id="spe_id' + count +
                    '"  value="' + specification_val + '" /></td>';
                // output += '<td class="left">' + specification_Desc +' <input type="hidden" name="description[]" id="spe_description' + count +'"  value="' + specification_Desc + '" /></td>';
                output += `<td class="left">${specification_Desc} <input type="hidden" name="description[]" id="spe_description${count}"  value='${specification_Desc}' /></td>`;
                output += '<td>';
                output +=
                    '<span class="d-inline-block back_col view_details" tabindex="0" data-toggle="tooltip" title="Edit Invoice" id="' +
                    count + '">';
                output += '<a class="btn btn-primary p-2 mr-1">';
                output += '<i class="fa fa-pencil-alt" style="color:white;" aria-hidden="true"></i>';
                output += '</a>';
                output += '</span>';
                output +=
                    '<span class="d-inline-block back_col remove_details" tabindex="0" data-toggle="tooltip" title="Delete Invoice" id="' +
                    count + '">';
                output += '<a class="btn btn-danger p-2">';
                output += '<i class="fa fa-trash" style="color:white;" aria-hidden="true"></i>';
                output += '</a>';
                output += '</span>';
                output += '</td>';
                output += '</tr>';
                $('#product_data').append(output);
                $("#sp_name").val(""), $("#sp_description").val("");
                CKEDITOR.instances.sp_description.updateElement();
                CKEDITOR.instances.sp_description.setData('');  
                $("#save_property").removeClass("d-none");
            }
        });
        //Display model popup selected row data
        $(document).on('click', '.view_details', function() {
            var row_id = $(this).attr("id");
            var sp_name = $("#spe_id" + row_id).val(),
                sp_desc = $("#spe_description" + row_id).val();
            console.log("sp_name" + sp_name);
            $('#edit_sp_name option[value=' + sp_name + ']').attr('selected', 'selected');
            $('#edit_sp_name').val(sp_name);
            $('#edit_description').val(sp_desc);
            CKEDITOR.instances.edit_description.setData(sp_desc );
            $('#edit_specification input[name="row_id"]').val(row_id);
            $('#edit_specification').modal('show');

        });
        //Edit Specific Row
        $(document).on('click', '#edit', function() {
            var isHide = (isShowHide) ? "d-none" : "";
            // 		var tax = $('#edit_tax_id').children("option:selected").val();

            var sp_name = $('#edit_sp_name').children("option:selected").text(),
                specification_val = $('#edit_sp_name').children("option:selected").val(),
                sp_description = CKEDITOR.instances.edit_description.getData();

            console.log("sp_description" + sp_description);
            console.log("row_id" + row_id);
            var row_id = $('#edit_specification input[name="row_id"]').val();
            output = '';
            output += '<th>' + row_id + '</th>';
            output += '<td class="left">' + sp_name +' <input type="hidden" name="name[]" id="spe_name' + row_id + '" value="' + sp_name +'" /><input type="hidden" name="spe_id[]" id="spe_id' + row_id + '"  value="' + specification_val + '" /></td>';
            
            // output += '<td class="left">"' + sp_description + '" <input type="hidden" name="description[]" id="spe_description' + row_id +'"  value="' + sp_description + '"/> </td>';
            output += `<td class="left">${sp_description} <input type="hidden" name="description[]" id="spe_description${row_id}" value='${sp_description}' /></td>`;
            output += '<td>';
            output +=
                '<span class="d-inline-block back_col view_details" tabindex="0" data-toggle="tooltip" title="Edit Specification" id="' +
                row_id + '">';
            output += '<a class="btn btn-primary p-2 mr-1">';
            output += '<i class="fa fa-pencil-alt" style="color:white;" aria-hidden="true"></i>';
            output += '</a>';
            output += '</span>';
            output +=
                '<span class="d-inline-block back_col remove_details" tabindex="0" data-toggle="tooltip" title="Delete Specification" id="' +
                row_id + '">';
            output += '<a class="btn btn-danger p-2">';
            output += '<i class="fa fa-trash"  style="color:white;" aria-hidden="true"></i>';
            output += '</a>';
            output += '</span>';
            output += '</td>';
            $('#row_' + row_id + '').html(output);
            $("#edit_specification .close").click();
            CKEDITOR.instances.sp_description.updateElement();
            CKEDITOR.instances.sp_description.setData('');
        });

        // Remove selected Row
        $(document).on('click', '.remove_details', function() {
            var row_id = $(this).attr("id");
            if (confirm("Are you sure you want to remove this Specification data?")) {
                $('#row_' + row_id + '').remove();
                if ($(".new_row").length <= 0) {
                    count = 1;
                    var row = '';
                    row = '<tr id="empty_table">';
                    row +=
                        '<td id="empty_table_colspan" colspan="13">You have not added any Specification yet.</td>';
                    row += '</tr>';
                    $('#product_data').append(row);
                    $("#save_property").addClass("d-none");
                }
                count--;

            } else {
                return false;
            }
        });

        //flat
        var isShowHide = false;
        var fcount = '{{$totalflatRes}}';
        $('#addflat').click(function() {
            //set error message product/service and client
            var error_floor = '',
                error_sqft = '',
                error_bhk = '',
                error_status = '';
            status = $('#status').children("option:selected").val();
            if ($('#floor').val() == '') {
                error_floor = 'enter floor';
                $('#err_floor').text(error_floor);
                $('#floor').css('border-color', '#cc0000');
            } else {
                error_floor = '';
                $('#err_floor').text(error_floor);
                $('#floor').css('border-color', '');
            }
            if ($('#sqft').val() == '') {
                error_sqft = 'Enter Square Fit';
                $('#err_sqft').text(error_sqft);
                $('#sqft').css('border-color', '#cc0000');
            } else {
                error_sqft = '';
                $('#err_sqft').text(error_sqft);
                $('#sqft').css('border-color', '');
            }
            if ($('#bhk').val() == '') {
                error_bhk = 'Enter BHk';
                $('#err_bhk').text(error_bhk);
                $('#bhk').css('border-color', '#cc0000');
            } else {
                error_bhk = '';
                $('#err_bhk').text(error_bhk);
                $('#bhk').css('border-color', '');
            }
            if (status == '') {
                error_status = 'Enter Square Fit';
                $('#err_status').text(error_status);
                $('#status').css('border-color', '#cc0000');
            } else {
                error_status = '';
                $('#err_status').text(error_status);
                $('#status').css('border-color', '');
            }
            if (error_floor != '' || error_sqft != '' || error_bhk != '' || error_status != '')
                return false;
            else {

                $("#empty_table").remove();

                var floor =  $('#floor').children("option:selected").val(),
                    sqft = $('#sqft').val(),
                    bhk = $("#bhk").val(),
                    status_name = $('#status').children("option:selected").text(),
                    status = $('#status').children("option:selected").val();

                fcount = (parseInt(fcount) + parseInt(1));
                output = '<tr id="frow_' + fcount + '" class="new_row">';
                output += '<th>' + fcount + '</th>';
                output += '<td class="left">' + floor +
                    ' <input type="hidden" name="floor[]" id="floor' + fcount + '"  value="' + floor +
                    '" /></td>';
                output += '<td class="left">' + sqft + ' <input type="hidden" name="sqft[]" id="sqft' +
                    fcount + '"  value="' + sqft + '" /></td>';
                output += '<td class="left">' + bhk + ' <input type="hidden" name="bhk[]" id="bhk' +
                    fcount + '"  value="' + bhk + '" /></td>';
                output += '<td class="left">' + status_name +
                    ' <input type="hidden" name="status[]" id="status' + fcount + '"  value="' +
                    status + '" /></td>';
                output += '<td>';
                output +=
                    '<span class="d-inline-block back_col view_flat_details" tabindex="0" data-toggle="tooltip" title="Edit flat" id="' +
                    fcount + '">';
                output += '<a class="btn btn-primary p-2 mr-1">';
                output += '<i class="fa fa-pencil-alt" style="color:white;" aria-hidden="true"></i>';
                output += '</a>';
                output += '</span>';
                output +=
                    '<span class="d-inline-block back_col remove_flat_details" tabindex="0" data-toggle="tooltip" title="Delete flat" id="' +
                    fcount + '">';
                output += '<a class="btn btn-danger p-2">';
                output += '<i class="fa fa-trash" style="color:white;" aria-hidden="true"></i>';
                output += '</a>';
                output += '</span>';
                output += '</td>';
                output += '</tr>';
                $('#flat_data').append(output);
                $("#floor").val(""), $("#sqft").val(""), $("#bhk").val(""), $("#status").val("");

            }
        });
        //Display model popup selected row data
        $(document).on('click', '.view_flat_details', function() {

            var row_id = $(this).attr("id");
            var floor =  $('#floor'+row_id).val(),
                sqft = $("#sqft" + row_id).val(),
                bhk = $("#bhk" + row_id).val(),
                status = $("#status" + row_id).val();

            $('#edit_status option[value=' + status + ']').attr('selected', 'selected');
            $('#edit_sqft').val(sqft);
            $('#edit_bhk').val(bhk);
            $('#edit_floor option[value='+floor+']').attr('selected','selected'),
            $('#edit_flat input[name="row_id"]').val(row_id);
            $('#edit_flat').modal('show');

        });
        //Edit Specific Row for flat
        $(document).on('click', '#edit_flat_save', function() {
            var isHide = (isShowHide) ? "d-none" : "";
            // 		var tax = $('#edit_tax_id').children("option:selected").val();

            var status_name = $('#edit_status').children("option:selected").text(),
                status = $('#edit_status').children("option:selected").val(),
                floor = $('#edit_floor').children("option:selected").val(),
                sqft = $("#edit_sqft").val(),
                bhk = $("#edit_bhk").val();


            var row_id = $('#edit_flat input[name="row_id"]').val();
            output = '';
            output += '<th>' + row_id + '</th>';
            output += '<td class="left">' + floor + ' <input type="hidden" name="floor[]" id="floor' +
                row_id + '" value="' + floor + '" /></td>';
            output += '<td class="left">' + sqft + ' <input type="hidden" name="sqft[]" id="sqft' +
                row_id + '"  value="' + sqft + '" /> </td>';
            output += '<td class="left">' + bhk + ' <input type="hidden" name="bhk[]" id="bhk' +
                row_id + '"  value="' + bhk + '" /> </td>';
            output += '<td class="left">' + status_name +
                ' <input type="hidden" name="status[]" id="status' + row_id + '"  value="' + status +
                '" /> </td>';
            output += '<td>';
            output +=
                '<span class="d-inline-block back_col view_flat_details" tabindex="0" data-toggle="tooltip" title="Edit flat" id="' +
                row_id + '">';
            output += '<a class="btn btn-primary p-2 mr-1">';
            output += '<i class="fa fa-pencil-alt" style="color:white;" aria-hidden="true"></i>';
            output += '</a>';
            output += '</span>';
            output +=
                '<span class="d-inline-block back_col remove_flat_details" tabindex="0" data-toggle="tooltip" title="Delete flat" id="' +
                row_id + '">';
            output += '<a class="btn btn-danger p-2">';
            output += '<i class="fa fa-trash"  style="color:white;" aria-hidden="true"></i>';
            output += '</a>';
            output += '</span>';
            output += '</td>';
            $('#frow_' + row_id + '').html(output);
            $("#edit_flat .close").click();

        });
        // Remove selected Row
        $(document).on('click', '.remove_flat_details', function() {
            var row_id = $(this).attr("id");
            if (confirm("Are you sure you want to remove this flat?")) {
                $('#frow_' + row_id + '').remove();
                if ($(".new_row").length <= 0) {
                    fcount = 1;
                    var row = '';
                    row = '<tr id="empty_table">';
                    row +=
                        '<td id="empty_table_colspan" colspan="13">You have not added any Flat yet.</td>';
                    row += '</tr>';
                    $('#flat_data').append(row);

                }
                fcount--;

            } else {
                return false;
            }
        });
        $("#add_image").click(function (e) {
            var html = "";
            var img = $('#imageData').children('.image').length;
            img = parseInt(img) + parseInt(1) ;
            html = `<div class="image">
                    <img src="{{ asset('public/images/no-image.png') }}" width="100"
                        height="100" id="artPrevImage_${img}" class="image"
                        style="margin-bottom:0px;padding:3px;" alt="logo" /><br />
                    <input type="file" name="image[]" id="ariImg_${img}"
                        onchange="readURL(this,'${img}');" style="display: none;"
                        accept="image/jpg,image/png">
                    <input type="hidden" value="" name="image" id="hiddenArtImgLogo" />
                    <a onclick="$('#ariImg_${img}').trigger('click');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style="clear:both;"
                        onclick="javascript:clear_image('artPrevImage_${img}')">Clear</a></div>`;
            $("#imageData").append(html);

        });

    });

    $(window).ready( ()=>{
        var country_id = $("#country").children("option:selected").val();
        $.ajax({
            type: "get",
            url: base_url+'/getState/'+country_id,
            success: function (response) {
                $("#state").empty();
                $.each(response, function (i, v) { 
                    $("#state").append(`<option value='${v.state_id}' ${(state == v.state_id) ? 'selected' : ''} >${v.state_name}</option>`);
                });
                displayCity();
            }
        });
    });
    function displayCity() { 
            var state_id = $("#state").children("option:selected").val();
                console.log('state_id'+state_id);
                $.ajax({
                    type: "get",
                    url: base_url+'/getcity/'+state_id,
                    success: function (response) {
                        $("#city").empty();
                        $.each(response, function (i, v) { 
                            $("#city").append(`<option value='${v.city_id}'  ${(city == v.city_id) ? 'selected' : ''} >${v.city_name}</option>`);
                        });
                        
                    }
                });
         }

         let ignore = false;

         $('.img-delete').click(function (e) { 
            let id = $(this).data('id');
            let parrentDiv = $(this).parent();
            let conirmation = confirm('Want to delete');
            $('body').addClass('is-Loder');
            if(conirmation){
                $.ajax({
                    type: "get",
                    url: `${url}/ssb-admn/property/img-delete/${id}`,
                    success: function (response) {
                        if(response.status == 1){
                            $(parrentDiv).remove();
                            $('body').removeClass('is-Loder');
                        }else{
                            alert('something went wrong please try again');
                        }
                    }
                });
            }else{
                return false;
            }
         });
        $('#propertyForm').submit(function (e) { 
            ignore = true;
        });
        $(function () {
            $(window).bind('beforeunload', function () {
                if (!ignore) {
                    return 'WARNING: Data you have entered may not be saved.';
                }
            });
        });
</script>
@endsection
