@extends('layouts.admin')
@section('content')
    <!-- / .main-navbar -->
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4 d-flex justify-content-start align-items-center">
            <div class="col-6 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Property List</h3>
            </div>
            <div class="col-6 col-sm-8 text-center text-sm-left mb-0 d-flex justify-content-end align-items-center">
                <a href="{{ url('ssb-admn/property/create') }}" class="bg-success rounded text-white text-center py-2 px-3 d-inline-block" style="box-shadow: inset 0 0 5px rgba(0,0,0,.2);">Add</a>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->
        @if(Session::has('message')) <div class="alert {{ Session::get('class') }} notification"><strong>{{ Session::get('message') }}</strong></div>@endif
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Active Users</h6>
                    </div>
                    <div class="card-body p-0 pb-3">
                        <table class="table mb-0 datatable">
                            <thead class="bg-light">
                                <tr>
                                    <th scope="col" class="border-0">Id</th>
                                    <th scope="col" class="border-0">Name</th>
                                    <th scope="col" class="border-0">Address</th>
                                    <th scope="col" class="border-0">Image</th>
                                    <th scope="col" class="border-0">S.Order</th>
                                    <th scope="col" class="border-0">Action</th>
                                </tr>
                            </thead>
                            <tbody id="table_body">
                             
                                @if (!empty($propertyArr))
                                    @foreach ($propertyArr as $k=>$item)
                                        <tr id="tr_{{$k+1}}">
                                            <td>{{$k+1}}</td>
                                            <td>{{ $item->name}}</td>
                                            <td>{{ $item->address}} </td>
                                            <td>
                                            	<?php 
                                            	if( isset( $item->images ) && !empty( $item->images ) )
                                            	    echo '<img class="icon" src="'.asset("storage/app/".$item->images['image']).'">';
                                            	else
                                            	    echo '<img class="icon" src="'.asset("public/app/images/no-image.png").'">';
                                            	?> 
                                        	</td>
                                            <td class="sort" id="{{$k+1}}">
                                            <label id="lable{{$k+1}}" class="label">{{$item['short_id']}}</label>
                                            <input type="text"  value="{{$item['short_id']}}" class="sort_order d-none" id="input{{$k+1}}" data-pid="{{$item['id']}}" >
                                            </td>
                                        <td><a href="{{url('ssb-admn/property/edit/'.$item->id)}}"><i class="far fa-edit"></i></i></a> <a href="{{url('ssb-admn/property/delete/'.$item->id)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection
@section('script')
    <script>
         var url = '{{url('/')}}';
        //  $(document).mouseup(function(e) 
        // {
        //    console.log('fun call');
        //     var container = $(".sort_order");

        //     // if the target of the click isn't the container nor a descendant of the container
        //     if (!container.is(e.target) && container.has(e.target).length === 0) 
        //     {
        //        $('.sort_order').attr('readony',true);
        //     }
        // });
         
        $(document).ready(function () { 
            $(document).on('click','.sort',function(){
                $('.label').removeClass('d-none');
                $('.sort_order').addClass('d-none');
                var inputId  = $(this).attr("id");
                $(`#lable${inputId}`).addClass("d-none");
                $(`#input${inputId}`).removeClass("d-none");
            });
        });

        $(document).on('change','.sort_order', function () {
            var sort = $(this).val();
            var product_id = $(this).data('pid');
            $.ajax({
                type: "get",
                url:  url+'/ssb-admn/property/sort-order/'+sort+'/'+product_id,
                    success: function (response) {
                        if(response.status == 1) location.reload();
                    }
                });
        });
        
    </script>
@endsection