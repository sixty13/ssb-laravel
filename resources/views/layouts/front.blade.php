<!DOCTYPE html>
<html lang="en">
  <head>

    <title>Sri Suprabhatham Builder | We shape your Future</title
><!-- Squirrly SEO Plugin 8.4.06, visit: https://plugin.squirrly.co/ -->

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="robots" content="index,follow">
<meta name="description" content="| Sri Suprabhatham Builder">
<link rel="canonical" href="https://www.srisuprabhathambuilder.com">
<link rel="alternate" type="application/rss+xml" href="https://www.srisuprabhathambuilder.com/sitemap.xml">

<meta name="dc.language" content="en-US">
<meta name="dc.language.iso" content="en_US">
<meta name="dc.publisher" content="Sri Suprabhatham Builder">
<meta name="dc.title" content="Sri Suprabhatham Builder | We shape your Future">
<meta name="dc.description" content="| Sri Suprabhatham Builder">
<meta name="dc.date.issued" content="2020-05-24">
<!-- /Squirrly SEO Plugin -->

    <!-- Favicon -->
<link rel="apple-touch-icon" sizes="57x57" href="{{asset('public/front/images/apple-icon-57x57.png')}}">
<link rel="apple-touch-icon" sizes="60x60" href="{{asset('public/front/images/apple-icon-60x60.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('public/front/images/apple-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{asset('public/front/images/apple-icon-76x76.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('public/front/images/apple-icon-114x114.png')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{asset('public/front/images/apple-icon-120x120.png')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{asset('public/front/images/apple-icon-144x144.png')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{asset('public/front/images/apple-icon-152x152.png')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('public/front/images/apple-icon-180x180.png')}}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{asset('public/front/images/android-icon-192x192.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('public/front/images/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{asset('public/front/images/favicon-96x96.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('public/front/images/favicon-16x16.png')}}">

<link href="http://fancyapps.com/fancybox/source/jquery.fancybox.css">

<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff"> 

    <link rel="icon" type="image/png" href="{{asset('public/front/images/sri-logo.png')}}" sizes="16x16">

    <!-- Google Font -->
     <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,800&display=swap" rel="stylesheet">

    <!-- CSS Global Compulsory (Do not remove)-->
    <link rel="stylesheet" href="{{asset('public/front/css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('public/front/css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('public/front/css/bootstrap/bootstrap.min.css')}}" />

    <!-- Page CSS Implementing Plugins (Remove the plugin CSS here if site does not use that feature)-->
    <link rel="stylesheet" href="{{asset('public/front/css/select2/select2.css')}}" />
    <link rel="stylesheet" href="{{asset('public/front/css/range-slider/ion.rangeSlider.css')}}" />
    <link rel="stylesheet" href="{{asset('public/front/css/owl-carousel/owl.carousel.min.css')}}" />
    <link rel="stylesheet" href="{{asset('public/front/css/magnific-popup/magnific-popup.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/css/animate/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/css/slick/slick-theme.css')}}" />
    <link rel="stylesheet" href="{{asset('public/front/css/owl.carousel.css')}}" />
    <!-- Template Style -->
    <link rel="stylesheet" href="{{asset('public/front/css/style.css')}}" />  
    <link href="{{asset('public/css/lightbox.css')}}" rel="stylesheet" />
    
    @yield('css')
    
  </head>

<body>
<!--== header -->
<header class="header">
  <div class="topbar">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="d-block d-md-flex align-items-center text-center">
            <div class="mr-3 d-inline-block">
              <a href="tel:1-800-555-1234"><i class="fa fa-phone mr-2 fa fa-flip-horizontal"></i>1-800-555-1234 </a>
            </div>
            <div class="mr-auto d-inline-block">
              <span class="mr-2 text-white">Get App:</span>
              <a class="pr-1" href="#"><i class="fab fa-android"></i></a>
              <a href="#"><i class="fab fa-apple"></i></a>
            </div>
            <div class="dropdown d-inline-block pl-2 pl-md-0">
              <a class="dropdown-toggle" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choose location<i class="fas fa-chevron-down pl-2"></i>
              </a>
              <div class="dropdown-menu mt-0" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Global</a>
                <a class="dropdown-item" href="#">Arizona</a>
                <a class="dropdown-item" href="#">British columbia</a>
                <a class="dropdown-item" href="#">Commercial</a>
              </div>
            </div>
            <div class="social d-inline-block">
              <ul class="list-unstyled">
                <li><a href="#"> <i class="fab fa-facebook-f"></i> </a></li>
                <li><a href="#"> <i class="fab fa-twitter"></i> </a></li>
                <li><a href="#"> <i class="fab fa-linkedin"></i> </a></li>
                <li><a href="#"> <i class="fab fa-pinterest"></i> </a></li>
                <li><a href="#"> <i class="fab fa-instagram"></i> </a></li>
              </ul>
            </div>
            <div class="login d-inline-block">
              <a data-toggle="modal" data-target="#loginModal" href="#">Hello sign in<i class="fa fa-user pl-2"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <nav class="navbar navbar-light bg-white navbar-static-top navbar-expand-lg header-sticky">
        <div class="container">

            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target=".navbar-collapse"><i class="fas fa-align-left"></i></button>
                <a class="navbar-brand" href="{{url('/')}}">
                <img class="img-fluid" src="{{asset('public/front/images/sri-logo.png')}}" alt="logo">
            </a>
            <ul class="nav justify-content-center d-lg-none social-set-icon">
              <li class="nav-item">
                <a class="nav-link call-icon whatsapp-call" href="https://api.whatsapp.com/send?phone=+919962482979" target="_blank"  aria-haspopup="true" aria-expanded="false">
                  <img src="{{asset('public/front/images/whatsapp.svg')}}" alt="whatsapp" class="whatsapp-img wh-icon" />
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link call-icon call-left" href="tel:+919962482979"  aria-haspopup="true" aria-expanded="false">
                                  <img src="{{asset('public/front/images/call.svg')}}" alt="whatsapp" class="call-img" />
                </a>
              </li>
            </ul>
            <div class="navbar-collapse collapse justify-content-center">
                <ul class="nav navbar-nav">
                  <li class="nav-item dropdown "> <!--active-->
                    <a class="nav-link" href="{{url('/')}}" id="navbarDropdown" role="button"  aria-haspopup="true" aria-expanded="false">Home</a>  
                  </li>
                  <li class="dropdown nav-item {{ (strpos(url()->full(), '/property-list') !== false) ? 'active' : '' }}">
                    <a href="{{url('property-list')}}" class="nav-link">Properties</a>
                  </li>
                  <li class="nav-item {{ (strpos(url()->full(), '/jv') !== false) ? 'active' : '' }} ">
                  <a class="nav-link " href="{{url('jv')}}"  aria-haspopup="true" aria-expanded="false">JV</a>
                  </li>
                  <li class="nav-item  d-none {{ (strpos(url()->full(), '/gallery') !== false) ? 'active' : '' }} ">
                  <a class="nav-link " href="{{url('gallery')}}"  aria-haspopup="true" aria-expanded="false">Gallery</a>
                  </li>
                  <li class="nav-item {{ (strpos(url()->full(), '/bunglow') !== false) ? 'active' : '' }}">
                    <a class="nav-link " href="{{url('bunglow')}}"  aria-haspopup="true" aria-expanded="false">Bunglow</a>
                  </li>
                  <li class="nav-item  {{ (strpos(url()->full(), '/about-us') !== false) ? 'active' : '' }}">
                    <a class="nav-link " href="{{url('about-us')}}"  aria-haspopup="true" aria-expanded="false">About Us</a>
                  </li>
                  <li class="nav-item {{ (strpos(url()->full(), '/contact-us') !== false) ? 'active' : '' }}">
                  <a class="nav-link " href="{{url('contact-us')}}"  aria-haspopup="true" aria-expanded="false">Contact Us</a>
                  </li>
                  <li class="nav-item d-none d-lg-block">
                    <a class="nav-link call-icon whatsapp-call" href="https://api.whatsapp.com/send?phone=+919962482979" target="_blank"  aria-haspopup="true" aria-expanded="false">
                      <img src="{{asset('public/front/images/whatsapp.svg')}}" alt="whatsapp" class="whatsapp-img" />
                    </a>
                  </li>
                  <li class="nav-item d-none d-lg-block mt-1">
                    <a class="nav-link call-icon call-left" href="tel:+919962482979"  aria-haspopup="true" aria-expanded="false">
                      <img src="{{asset('public/front/images/call.svg')}}" alt="whatsapp" class="call-img" />
                    </a>
                  </li>
                  
                </ul> 
            </div>
        </div>
    </nav>
</header>
<!--============  header -->
@yield('content')

<!--======= Meet our agent -->
<section class="py-5 bg-primary">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-9">
        <h2 class="text-white mb-0">Are You A Property Land Owner ?</h2>
      </div>
      <div class="col-lg-3 text-lg-right mt-3 mt-lg-0">
        <a class="btn btn-white" href="{{url('jv')}}">Click Here</a>
      </div>
    </div>
  </div>
</section>

<!--============ footer -->
<footer class="footer bg-dark space-pt">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <div class="footer-contact-info">
          <h5 class="text-primary mb-4">Sri Suprabhatham Builder</h5>
          
          <ul class="list-unstyled mb-0">
            <li> <b> <i class="fas fa-map-marker-alt"></i></b><span><strong>Brance Office:</strong> No.21/1, 21st street,4th main road, nanganallur,Chennai - 600 061.</span> </li>
            <li> <b> <i class="fas fa-map-marker-alt"></i></b><span><strong> Head Office:</strong>     No:19/9, Thambiah Reddy Road, Westmambalam, Chennai - 600033</span> </li>
            
            <li> <a href="tel: 04422246564"><b><i class="fas fa-phone"></i></b><span> 044-22246564/ 42648288</span> </a></li>
            <li> <a href="mailto:srisuprabhathambuilders@gmail.com"><b><i class="far fa-envelope"></i></b><span>srisuprabhathambuilders@gmail.com</span> </a></li>
            <li><b><i class="fab fa-skype"></i></b><span>Sri Suprabhatham Builder</span> </li>
          </ul>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
        <div class="footer-recent-List">
          <h5 class="text-primary mb-4">Recently listed properties</h5>
          <ul class="list-unstyled mb-0">
            <?php $property = GetProperty() ?>
            @foreach ($property as $k=>$item)
             
           @if($k < 2)
           @php 
              $labelAr = explode(",",$item['label']); @endphp
            <li>
              <div class="footer-recent-list-item">
                <?php 
                  if( isset( $item->images ) && !empty( $item->images ) )
                      echo '<img class="icon" src="'.asset("storage/app/".$item->images['image']).'">';
                  else
                      echo '<img class="icon" src="'.asset("public/images/no-image.png").'">';
                  ?> 
                <div class="footer-recent-list-item-info">
                  <h6 class="text-white"><a class="category font-md mb-2" href="{{url('property-detail/'.$item->id)}}">{{$item->name}}</a></h6>
                <a class="address mb-2 font-sm" href="{{url('property-detail/'.$item->id)}}">{{$item->address}}</a>
                  {{-- <span class="price text-white">₹{{$item->amount}} </span> --}}
                </div>
              </div>
            </li>
            @endif
            @endforeach
          </ul>
        </div>
      </div>
      
      <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
        <div class="footer-link">
          <h5 class="text-primary mb-4">Useful links</h5>
          <ul class="list-unstyled mb-0">
            <li> <a href="{{url('/')}}">Home</a> </li>
            <li> <a href="{{url('property-grid')}}">Properties </a> </li>
            <li> <a href="{{url('jv')}}">JV</a> </li>
            {{-- <li> <a href="{{url('gallery')}}">Gallery</a> </li> --}}
          {{-- </ul>
          <ul class="list-unstyled mb-0"> --}}
            <li> <a href="{{url('bunglow')}}">Bunglow </a> </li>
            <li> <a href="{{url('about-us')}}">About Us </a> </li>
            <li> <a href="{{url('contact-us')}}">Contact Us </a> </li>
            <li> <a href="#" style="visibility:hidden;">Newport</a> </li>
          </ul>
        </div>
      </div>
      
      <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
        <h5 class="text-primary mb-4">Subscribe us</h5>
        <div class="footer-subscribe">
          <p class="text-white">Sign up to our newsletter to get the latest news and offers.</p>
          @if(Session::has('semail')) <div class="alert alert-success"><strong>{{ Session::get('semail') }}</strong></div>@endif
          <form method="post" action="{{url('SubscribeUs')}}">
            @csrf
            <div class="form-group">
              <input type="email" name="semail" class="form-control" placeholder="Enter email">
              @error('semail')
                <em class="text-danger">{{$message}}</em>
              @enderror
            </div>
            <div id="recaptcha_subscribe" class="captcha-contact col-md-12 g-recaptcha"></div> 
            <em class="text-danger" id="recaptcha_subscribe_msg"></em>    
            <button type="submit" class="btn btn-primary btn-sm" id="subscribe_btn">Get notified</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-4 text-center text-md-left">
          <a href="{{url('')}}"><img class="img-fluid footer-logo" src="{{asset('public/front/images/sri-logo.png')}}" alt="">
            <span class="ml-2 text-white">©Copyright 2020 SSB All Rights Reserved</span>
           </a>
        </div>
        <div class="col-md-4 text-center my-3 mt-md-0 mb-md-0">
          <a id="back-to-top" class="back-to-top" href="#"><i class="fas fa-angle-double-up"></i> </a>
        </div>
        <div class="col-md-4 text-center text-md-right">
          <p class="mb-0 text-white"> &copy;Copyright <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script></span> <a href="#"> SSB </a> All Rights Reserved </p>

        </div>
      </div>
    </div>
  </div>
</footer>
<!--=================================
footer-->

<!--=================================
Javascript -->

  <!-- JS Global Compulsory (Do not remove)-->
  <script src="{{asset('public/front/js/jquery-3.4.1.min.js')}}"></script>
  <script src="{{asset('public/front/js/popper/popper.min.js')}}"></script>
  <script src="{{asset('public/front/js/bootstrap/bootstrap.min.js')}}"></script>

  <!-- Page JS Implementing Plugins (Remove the plugin script here if site does not use that feature)-->
  <!-- <script src="{{asset('public/front/js/smoothscroll.js')}}"></script> -->
  <script src="{{asset('public/front/js/jquery.appear.js')}}"></script>
  <script src="{{asset('public/front/js/counter/jquery.countTo.js')}}"></script>
  <script src="{{asset('public/front/js/select2/select2.full.js')}}"></script>
  <script src="{{asset('public/front/js/range-slider/ion.rangeSlider.min.js')}}"></script>
  <script src="{{asset('public/front/js/owl-carousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('public/front/js/jarallax/jarallax.min.js')}}"></script>
  <script src="{{asset('public/front/js/jarallax/jarallax-video.min.js')}}"></script>
  
  <!-- <script src="{{asset('public/front/js/magnific-popup/jquery.magnific-popup.min.js')}}">
  </script> -->

  <script src="{{asset('public/front/js/slick/slick.min.js')}}"></script>
  <script src="{{asset('public/front/js/jssor.slider-28.0.0.min.js')}}" type="text/javascript">
  </script>
  <script src="{{asset('public/js/lightbox.js')}}"></script>

  <script src="{{asset('public/front/js/jquery.stellar.min.js')}}"></script>

  <script src="{{asset('public/front/js/jquery.magnific-popup.js')}}"></script>

  <script src="{{asset('public/front/js/owl.carousel.min.js')}}" type="text/javascript">
  </script>
  <script src="{{asset('public/front/js/interface.js')}}"></script>
  <!-- <script src="http://fancyapps.com/fancybox/source/jquery.fancybox.js"></script> -->


<!-- SLIDER REVOLUTION -->

<script src="{{asset('public/front/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('public/front/js/jquery.themepunch.revolution.min.js')}}"></script> 

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS   -->
<script src="{{asset('public/front/js/revolution.extension.actions.min.js')}}"></script>
<script src="{{asset('public/front/js/revolution.extension.carousel.min.js')}}"></script>
<script src="{{asset('public/front/js/revolution.extension.kenburn.min.js')}}"></script>
<script src="{{asset('public/front/js/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{asset('public/front/js/revolution.extension.migration.min.js')}}"></script>
<script src="{{asset('public/front/js/revolution.extension.navigation.min.js')}}"></script>
<script src="{{asset('public/front/js/revolution.extension.parallax.min.js')}}"></script>
<script src="{{asset('public/front/js/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{asset('public/front/js/revolution.extension.video.min.js')}}"></script> 

  <!-- Template Scripts (Do not remove)-->
  <script src="{{asset('public/front/js/custom.js')}}"></script>
  <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
  </script>
  <script>
    var flag =  false;
$("#subscribe_btn").click(function (e) { 

  if(flag)
  {
    $("#recaptcha_subscribe_msg").text("");
  }
  else
  {
    $("#recaptcha_subscribe_msg").text("Please checked Recaptcha");
      return false;
  }
});

var verifyCallback = function(response) {
     flag  = true;
     $("#recaptcha_subscribe_msg").text("");
  };
  $("#joint_developemnt_form").click(function (e) { 

if(flag)
{
$("#recaptcha_msg").text("");
}
else
{
$("#recaptcha_msg").text("Please checked Recaptcha");
  return false;
}
});
 var widgetId1;
 var widgetId2;
//  var onloadCallback = function() {
//    // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
//    // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
//   grecaptcha.render('recaptcha_subscribe', {
//      'sitekey' : '6LdtCdkZAAAAAAvaXa1LuJLFBbhQLxSDXuaI0diG',
//      'callback' : verifyCallback,
//      'theme' : 'light'
//    });
//  };
 var onloadCallback = function() {
        var recaptchas = document.querySelectorAll('.g-recaptcha');

        for( i = 0; i < recaptchas.length; i++) {
            grecaptcha.render( recaptchas[i].id, {
            'sitekey' : '6LdtCdkZAAAAAAvaXa1LuJLFBbhQLxSDXuaI0diG',
            'callback' : verifyCallback,
            'theme' : 'light'
            });
        }
    }
</script>
  

  <script>
    $(".js-projects-carousel").owlCarousel({
      itemsMobile:[479,1],
      itemsTablet:[768,2],
      itemsDesktopSmall:[979,2],
      itemsDesktop:[1250,3],
      items:4,
      pagination:false,
      navigation:false,
      slideSpeed:700,
      responsiveRefreshRate:0,
      autoPlay: true
    });

    $(".owl-carousel-testimonial").owlCarousel({
      itemsMobile:[479,1],
      itemsTablet:[768,2],
      itemsDesktopSmall:[979,2],
      itemsDesktop:[1250,3],
      items:2,
      dots: true,
      loop:true,
      pagination:false,
      navigation:false,
      slideSpeed:5000,
      responsiveRefreshRate:0,
      autoPlay: true
    });

    $(".owl-carousel-bank").owlCarousel({
      itemsMobile:[479,1],
      itemsTablet:[768,2],
      itemsDesktopSmall:[979,2],
      itemsDesktop:[1250,3],
      items:4,
      dots: true,
      loop:true,
      pagination:false,
      navigation:false,
      slideSpeed:5000,
      responsiveRefreshRate:0,
      autoPlay: true
    });


  </script>
  
  @yield('script')
<script>
  
</script>
</body>
</html>