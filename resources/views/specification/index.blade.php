@extends('layouts.admin')
@section('content')
    <!-- / .main-navbar -->
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-6 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Specification Llist</h3>
            </div>
            <div class="col-6 col-sm-8 text-center text-sm-left mb-0 d-flex justify-content-end align-items-center">
                <a href="{{ url('ssb-admn/specification/create') }}" class="bg-success rounded text-white text-center py-2 px-3 d-inline-block" style="box-shadow: inset 0 0 5px rgba(0,0,0,.2);">Add</a>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->
        @if(Session::has('message')) <div class="alert {{ Session::get('class') }} notification"><strong>{{ Session::get('message') }}</strong></div>@endif
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Active Users</h6>
                    </div>
                    <div class="card-body p-0 pb-3">
                        <table class="table mb-0 datatable">
                            <thead class="bg-light">
                                <tr>
                                    <th scope="col" class="border-0">id</th>
                                    <th scope="col" class="border-0">name</th>
                                    <th scope="col" class="border-0">icon</th>
                                    <th scope="col" class="border-0">Description</th>
                                    <th scope="col" class="border-0">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($specificationArr))
                                    @foreach ($specificationArr as $k=>$item)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            <td>{{ $item->name }}</td>
                                            <td><img class="icon" src="{{asset('public/images/icon/'.$item->icon)}}" </td>
                                            <td>{!! $item->description !!}</td>
                                        <td><a href="{{url('ssb-admn/specification/edit/'.$item->id)}}"><i class="far fa-edit"></i></i></a> <a href="{{url('ssb-admn/specification/delete/'.$item->id)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Default Light Table -->

    </div>
@endsection
