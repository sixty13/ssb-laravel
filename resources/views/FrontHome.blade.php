@extends('layouts.front')
@section('content')
   
 <style>
 .d-none {display:none; }
 </style>
<!--=================================
banner -->
<section class="clearfix">
    <div id="slider" class="carousel slide" data-ride="carousel" data-interval="3000">
      {{-- <ol class="carousel-indicators">
        <li data-target="#slider" data-slide-to="0" class="active"></li>
        <li data-target="#slider" data-slide-to="1"></li>
        <li data-target="#slider" data-slide-to="2"></li>
        <li data-target="#slider" data-slide-to="3"></li>
        <li data-target="#slider" data-slide-to="4"></li>
      </ol> --}}
      <div class="carousel-inner">
        <div class="carousel-item active slider-img">
          <img class="img-fluid" src="{{asset('public/front/images/SWARG_WEBSITE.jpg')}}" alt="{{asset('public/front/images/Slider-1.jpg')}}">
          <div class="slider-content">
            <div class="container cont-set">
              <div class="row">
                <div class="col-lg-9 col-md-10 pad-left-slider">
                    <!-- <span class="text-white animated-07">Find your happy</span> -->
                    <h1 class="text-white mb-3 animated-08">SWARG</h1>
                    <p class="slider-text animated-08 mb-0"> Property 2 & 3 BHK Flats Bookings Open now</p>
                    <a href="{{url('property-detail/54')}}" class="btn btn-link animated-08">More Details <i class="fas fa-arrow-right pl-2"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="carousel-item slider-img">
          <img class="img-fluid" src="{{asset('public/front/images/WEBSITE-1920-X-1080.jpg')}}" alt="{{asset('public/front/images/Slider-1.jpg')}}">
          <div class="slider-content">
            <div class="container cont-set">
              <div class="row">
                <div class="col-lg-9 col-md-10 pad-left-slider">
                    <!-- <span class="text-white animated-07">Find your happy</span> -->
                    <h1 class="text-white mb-3 animated-08">NORTH EAST</h1>
                    <p class="slider-text animated-08 mb-0"> Property 3 BHK Flats Bookings Open now</p>
                    <a href="{{url('property-detail/53')}}" class="btn btn-link animated-08">More Details <i class="fas fa-arrow-right pl-2"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
		    <div class="carousel-item slider-img">
          <img class="img-fluid" src="{{asset('public/front/images/trident.jpg')}}" alt="{{asset('public/front/images/Slider-1.jpg')}}">
          <div class="slider-content">
            <div class="container cont-set">
              <div class="row">
                <div class="col-lg-9 col-md-10 pad-left-slider">
                    <!-- <span class="text-white animated-07">Find your happy</span> -->
                    <h1 class="text-white mb-3 animated-08">Warm Welcome</h1>
                    <p class="slider-text animated-08 mb-0"> Property 2 & 3 BHK Flats Bookings Open now</p>
                    <a href="{{url('property-detail/52')}}" class="btn btn-link animated-08">More Details <i class="fas fa-arrow-right pl-2"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="carousel-item slider-img">
          <img class="img-fluid" src="{{asset('public/front/images/Slider-1.jpg')}}" alt="">
          <!-- images/slider/01.jpg -->
          <div class="slider-content">
            <div class="container cont-set">
              <div class="row">
                <div class="col-lg-9 col-md-10 pad-left-slider">
                    <!-- <span class="text-white animated-07">Real villa</span> -->
                    <h1 class="text-white mb-3 animated-08">Welcome to Ojas</h1>
                    <p class="slider-text animated-08 mb-0">at Nanganallur 3 BHK From Rs 1,15,00,000</p>
                    <a href="{{url('property-detail/23')}}" class="btn btn-link animated-08">More Details <i class="fas fa-arrow-right pl-2"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
		
        <div class="carousel-item slider-img">
          <img class="img-fluid" src="{{asset('public/front/images/Slider-4.jpg')}}" alt="">
          <div class="slider-content">
            <div class="container srisupra_new cont-set">
              <div class="row">
                <div class="col-lg-9 col-md-10 pad-left-slider">
                    <!-- <span class="text-white animated-07">Find your happy</span> -->
                    <h1 class="text-white mb-3 animated-08">Welcome To Sarvah</h1>
                    <p class="slider-text animated-08 mb-0">Property 2& 3 BHK Flats Bookings Open now</p>
                    <a href="{{url('property-detail/26')}}" class="btn btn-link animated-08">More Details <i class="fas fa-arrow-right pl-2"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="d-none d-md-block">
        <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
    </div>
    </div>
  </section>
  
  <!--=========== banner -->
  
<!--=====Start About us -->
<section class="my-5">
    <div class="container">
        <div class="sri-builder">
            <h1>Welcome to Sri Suprabhatham Builder </h1>
            <p>M/s. Sri Suprabhatham builder was incorporated in 2002.The firm is engaged in the business of developing and constructions of flats over the last two decades. The firm also operates as flats promoters & developers.Mr.R.Ravichandran have been in to this business in the name of Sri Suprabhatham builder and managed by his son & himself. An M/S. Sri Suprabhatham builder is being managed by a group of professionals who are committed to provide qualitative and value-based apartments to satisfy the needs of the consumers.Rich collective experience of over 40 man-years in industry has provided us the imputes to render efficient and cost effective projects so as to cater to the demands of lower and upper middle class families.Our teams, our people are our greatest strengths .over a two decades of professional experience has given us what are today our core strengths viz.Strong projects Management Skills. Good academic Background, and strong working processes and methodologies. The promoters have had good consistent track record and the functioning of the company has consistently been impressive during the last few years.</p>
        </div>
    </div>
</section>
<!--=====End About Us-->
  <!--==========
    Featured properties-->
  <section class="space-pb">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-title text-center">
            <h2>Latest Property</h2>
            <p>Our latest projects in Chennai giving a Holistic Living Experience</p>
          </div>
        </div>
      </div>
      <div class="row">
        @foreach ($property as $item)
        @php  $labelAr = explode(",",$item['label']); @endphp
        
          <div class="col-sm-6 col-md-4">
              <div class="property-item">
                <div class="property-image bg-overlay-gradient-04">
                  <?php 
                  if( isset( $item->images ) && !empty( $item->images ) )
                      echo '<img class="icon" src="'.asset("storage/app/".$item->images['image']).'">';
                  else
                      echo '<img class="icon" src="'.asset("public/images/no-image.png").'">';
                  ?> 
                  <div class="property-lable">
                    @foreach ($labelAr as $ar)
                    <span class="badge badge-md badge-primary">{{$ar}}</span>
                   @endforeach
                    {{-- <span class="badge badge-md badge-info">Sale </span> --}}
                  </div>
                  <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>
                  {{-- <div class="property-agent">
                    <div class="property-agent-image">
                      <img class="img-fluid" src="{{asset('public/front/images/avatar/01.jpg')}}" alt="">
                    </div>
                    <div class="property-agent-info">
                      <a class="property-agent-name" href="#">Alice Williams</a>
                      <span class="d-block">Company Agent</span>
                      <ul class="property-agent-contact list-unstyled">
                        <li><a href="#"><i class="fas fa-mobile-alt"></i> </a></li>
                        <li><a href="#"><i class="fas fa-envelope"></i> </a></li>
                      </ul>
                    </div>
                  </div> --}}
                  <div class="property-agent-popup d-none">
                  {{-- <a href="#"><i class="fas fa-camera"></i>{{$count}}</a> --}}
                  </div>
                </div>
                <div class="property-details">
                  <div class="property-details-inner">
                  <h5 class="property-title"><a href="{{url('property-detail/'.$item->id)}}">{{$item->name}}</a></h5>
                  <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i>{{$item->address}}</span>
                  <div class="property-price">₹{{$item->amount}}</div>
                    <ul class="property-info list-unstyled d-flex">
                    <li class="flex-fill property-bed"><i class="fas fa-bed"></i>Bed<span>{{$item->bed}}</span></li>
                    <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span>{{$item->bath}}</span></li>
                    <li class="flex-fill property-m-sqft"><i class="far fa-square"></i>sqft<span>{{$item->sqft}}</span></li>
                    </ul>
                  </div>
                  <div class="property-btn">
                  <a class="property-link" href="{{url('property-detail/'.$item->id)}}">See Details</a>
                    <ul class="property-listing-actions list-unstyled mb-0">
                      <li class="property-compare"><a data-toggle="tooltip" data-placement="top" title="Compare" href="#"><i class="fas fa-exchange-alt"></i></a></li>
                      <li class="property-favourites"><a data-toggle="tooltip" data-placement="top" title="Favourite" href="#"><i class="far fa-heart"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div> 
          @endforeach
       
        <div class="col-12 text-center">
          <a class="btn btn-link" href="{{url('property-list')}}"><i class="fas fa-plus"></i>View All Listings</a>
        </div>
      </div>
    </div>
  </section>
  <!--=================================
  Featured properties-->
  
  <!--=================================
   offering the Best Real Estate-->
  <section class="clearfix">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-title text-center">
            <a href="{{url('bunglow')}}">
              <h2>Welcome to Kishore Homes</h2>
              <p style="color: #969696">Beautiful Bungalow craftsmanship by KH</p>
            </a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 p-0">
          <div class="layout">
            <div class="content">   
                <section id="projects" class="projects section">
                  <div class="section-content">
                    <div class="projects-carousel js-projects-carousel js-projects-gallery">  

                        <div class="project">
                        <a href="javascript:void(0)" title="project 3" data-toggle="modal" data-target="#vivekanand">
                          <figure>
                          <figure>
                            <img alt="" src="{{asset('public/front/images/Vivek/vivek(1).jpg')}}">
                            <figcaption>
                              <h3 class="project-title">
                                Residence Of Mr. Vivek Anand
                              </h3>
                              <h4 class="project-category">
                                Architecture
                              </h4>
                              <div class="project-zoom"></div>
                            </figcaption>
                          </figure>
                        </a>
                      </div>

                      <div class="project project-light">
                        <a href="javascript:void(0)" title="project 1" data-toggle="modal" data-target="#kumar">
                          <figure>
                            <img src="{{asset('public/front/images/Kumar/kumar_ssb (8).jpg')}}">
                            <figcaption>
                              <h3 class="project-title">
                                Residence Of Mr. Kumar
                              </h3>
                              <h4 class="project-category">
                                Architecture
                              </h4>
                              <div class="project-zoom"></div>
                            </figcaption>
                          </figure>
                        </a>
                      </div>

                      <div class="project">
                        <a href="javascript:void(0)" title="project 3" data-toggle="modal" data-target="#ravichandran">
                          <figure>
                          <figure>
                            <img alt="" src="{{asset('public/front/images/Ravichandran/Ravichandran(1).JPG')}}" class="w-100">
                            <figcaption>
                              <h3 class="project-title">
                                Residence Of Mr. Ravichandran
                              </h3>
                              <h4 class="project-category">
                                Architecture
                              </h4>
                              <div class="project-zoom"></div>
                            </figcaption>
                          </figure>
                        </a>
                      </div>
    
                      <div class="project project-light">
                        <a href="javascript:void(0)" title="project 2" data-toggle="modal" data-target="#murali">
                          <figure>
                            <img alt="" src="{{asset('public/front/images/Murali/murali (1).JPG')}}">
                            <figcaption>
                              <h3 class="project-title">
                                Residence Of Mr. Murali
                              </h3>
                              <h4 class="project-category">Architecture</h4>
                              <div class="project-zoom"></div>
                            </figcaption>
                          </figure>
                        </a>
                      </div>
                      

                    </div>
                  </div>
                </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

    <!--========  testimonial -->
  <section class="testimonial-main bg-holder mt-5" style="background-image: url({{asset('public/front/images/Ravichhandran.jpg')}});">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div>
            <div class="">
              <div class="testimonial">
                <div class="testimonial-content">
                  <p><i class="quotes">"</i>We value your Money and respect your time. Standing with us in good stead in these endeavors is our unwavering commitment to delivering nothing less than the very best, in terms of both quality and value.</p>
                </div>
                <div class="testimonial-name">
                  <h6 class="text-primary mb-1">MR.RAVICHANDRAN,</h6>
                  <span>Founder & Director.,</span>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--======== testimonial -->

    <!--=================================
  testimonial -->
  <section class="testimonial-main bg-holder mt-4" style="background-image: url({{asset('public/front/images/Kishore.jpg')}});">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div>
            <div class="">
              <div class="testimonial">
                <div class="testimonial-content">
                  <p><i class="quotes">"</i>Our Concepts and plans are crafted by renowned designers and architects, to provide  a dream home that's stylish with a modern layout to give our clients a luxurious Home for the best affordable price.</p>
                </div>
                <div class="testimonial-name">
                  <h6 class="text-primary mb-1">MR.KISHORE RAVICHANDRAN,</h6>
                  <span>Managing Director.,</span>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--=================================
  testimonial -->
    
<!--   <section class="testimonial-main bg-holder mt-5" style="background-image: url({{asset('public/front/images/bg/02.jpg')}});">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div>
            <div >
              <div class="testimonial">
                <div class="testimonial-content">
                  <p><i class="quotes">"</i>Thank you Martin for selling our apartment. We are delighted with the result. I can highly recommend Martin to anyone seeking a truly professional Realtor.</p>
                </div>
                <div class="testimonial-name">
                  <h6 class="text-primary mb-1">Lisa & Graeme</h6>
                  <span>Hamilton Rd. Willoughby</span>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <!--=================================
  testimonial -->
 
 <!--==== Meet our agent -->
<section class="mt-5 mb-5 cust-mar-bottom">
    <div class="section-title text-center">
          <h2>Testimonial</h2>
          <p>Our Most Valuable Testimonial Statements</p>
    </div>
    <div class="container">
        <div class="owl-carousel-testimonial owl-theme owl-dots-bottom-left" data-nav-dots="true">

            <div class="item">
              <div class="row">
                 <div class="col-md-4">
                   <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                        <img class="rounded-circle" src="{{asset('public/front/images/SRINATH  (SCRTEL ELECTRICS LTD  U.K).png')}}" alt="">
                    </div>
                 </div>
                 <div class="col-md-8">
                    <div class="agent-info">
                      <i class="fas fa-quote-left"></i>
                        <h6 class="mb-0"><a href="agent-detail.html"></a>Srinath</h6>
                        <span class="text-primary font-sm">SCRTEL ELECTRICS LTD  U.K</span>
                        <p class="mt-3 mb-0">Prior to choosing SSB, we walked through countless new construction homes. After seeing a Ravichandran Home, it was clear that the quality and attention to detail were next level. It was great working with Kishore and his team throughout the building process. They were easy to get in touch with and quick to respond to any questions that came up. The project management document that they provided was very transparent, made the selection process easy, and aided in communication throughout the entirety of the build. We would certainly recommend Sri Suprabhatham Builders and would be happy to work with them again in the future.
                        </p>
                      </div>
                 </div>
              </div>
            </div>
            
            <div class="item">
                <!-- <div class="agent text-center">
                    <div class="agent-detail">
                        <div class="agent-avatar avatar avatar-xllll">
                          <img class="img-fluid rounded-circle" src="images/profile-1.jpg" alt="">
                        </div>
                        <div class="agent-info">
                          <h6 class="mb-0"> <a href="agent-detail.html">Alice Williams </a></h6>
                          <span class="text-primary font-sm">Founder & CEO </span>
                          <p class="mt-3 mb-0">The first thing to remember about success is that it is a process – nothing more, nothing less.</p>
                        </div>
                    </div>
                </div> -->

                <div class="row">
                   <div class="col-md-4">
                     <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                          <img class="rounded-circle" src="{{asset('public/front/images/AMIRTHAVALLI ( ADVOCATE).png')}}" alt="" />
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="agent-info">
                        <i class="fas fa-quote-left"></i>
                          <h6 class="mb-0"> <a href="agent-detail.html">Amirthavalli</a></h6>
                          <span class="text-primary font-sm">Advocate </span>
                          <p class="mt-3 mb-0">
                            We had the privilege and honor of being the first client of Sri Suprabhatham Builder. With Mr. Ravichandran Dedication, Sincerity, and Honesty we understand that we made the right choice for buying our apartment from them. We wish them all the best and we are sure they will go a long way.
                          </p>
                        </div>
                   </div>
                </div>
            </div>

            <div class="item">
                <div class="row">
                   <div class="col-md-4">
                     <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                          <img class="rounded-circle" src="{{asset('public/front/images/BALASUBRAMANIAN (BHARATH ELECTRICAL LTD).png')}}" alt="">
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="agent-info">
                        <i class="fas fa-quote-left"></i>
                          <h6 class="mb-0"> <a href="agent-detail.html">Balasubramanian</a></h6>
                          <span class="text-primary font-sm">Bharath Electrical PVT LTD. </span>
                          <p class="mt-3 mb-0">We are very much pleased with the Management of SSB for their Transparency in Documentation & Legal aspects. They have good patience in hearing the Client’s point of view and lend them valuable Advice & Ideas for the project. The execution of the project is Time-Oriented. We wish them huge success for their future projects and i will definitely refer to my other family members and colleagues as well.
                          </p>
                        </div>
                   </div>
                </div>
            </div>

            <div class="item">
                <div class="row">
                   <div class="col-md-4">
                     <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                          <img class="rounded-circle" src="{{asset('public/front/images/Dr. ISHWARYA.png')}}" alt="">
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="agent-info">
                        <i class="fas fa-quote-left"></i>
                          <h6 class="mb-0"> <a href="agent-detail.html">Dr. Ishwarya </a></h6>
                          <span class="text-primary font-sm">Doctor</span>
                          <p class="mt-3 mb-0">If you are looking for a seasoned and skilled builder focused on honesty, integrity, fantastic communication, extreme attention to detail, and quality contractors... call Kishore Kannan! So impressed how they have built our home from idea in our heads to reality. Every day we are more impressed.
                          </p>
                        </div>
                   </div>
                </div>
            </div>

            <div class="item">
                <div class="row">
                   <div class="col-md-4">
                     <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                          <img class="rounded-circle" src="{{asset('public/front/images/G.MURUGAN (GOVCHE INDIA PVT LTD).png')}}" alt="">
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="agent-info">
                        <i class="fas fa-quote-left"></i>
                          <h6 class="mb-0"><a href="agent-detail.html">G.Murugan </a></h6>
                          <span class="text-primary font-sm">GOVCHE INDIA PVT LTD </span>
                          <p class="mt-3 mb-0">A fantastic builder with a reputation for being transparent and customer friendly. Approachable 24X7, they have a dedicated team for every department to ensure that quality is uncompromised. For SSB, it’s not merely a contract or sale but a relation, in which they ensure that they address any issue we may face at any point of time. I would definitely recommend SSB to all property buyers and vouch for the fact that SSB is a builder you can trust.
                          </p>
                        </div>
                   </div>
                </div>
            </div>

            <div class="item">
                <div class="row">
                   <div class="col-md-4">
                     <div class="agent-avatar avatar avatar-xllll verticle-testimonial">
                          <img class="rounded-circle" src="{{asset('public/front/images/SRIKANTHGOPALAN (SCRTEL ELECTRICS  DIRECTORATE.png')}}" alt="">
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="agent-info">
                        <i class="fas fa-quote-left"></i>
                          <h6 class="mb-0"><a href="agent-detail.html">Srikanthgopalan </a></h6>
                          <span class="text-primary font-sm">SCRTEL ELECTRICS  DIRECTORATE </span>
                          <p class="mt-3 mb-0">Prior to choosing SSB, we walked through countless new construction homes. After seeing a Ravichandran Home, it was clear that the quality and attention to detail were next level. It was great working with Kishore and his team throughout the building process. They were easy to get in touch with and quick to respond to any questions that came up. The project management document that they provided was very transparent, made the selection process easy, and aided in communication throughout the entirety of the build. We would certainly recommend Sri Suprabhatham Builders and would be happy to work with them again in the future.
                          </p>
                        </div>
                   </div>
                </div>
            </div>  
            
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-back" id="vivekanand" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-gallery">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            @for ($i = 1; $i <= 39; $i++)
            <div class="carousel-item {{($i == 1) ? 'active' : ''}}">
              <img class="d-block w-100" src="{{asset('public/front/images/Vivek/vivek('.$i.').jpg')}}" alt="First slide">
              <div class="slider-caption mt-2">
                <h4 style="color: #969696;">Residence Of Mr. Vivek Anand</h4>
              </div>
            </div>
            @endfor
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modal-back" id="murali" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-gallery">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">

        <div id="carouselmurali" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            @for ($i = 1; $i <= 12; $i++)
            <div class="carousel-item {{($i == 1) ? 'active' : ''}}">
              <img class="d-block w-100" src="{{asset('public/front/images/Murali/murali ('.$i.').JPG')}}" alt="First slide">
              <div class="slider-caption mt-2">
                <h4 style="color: #969696;">Residence Of Mr. Murali</h4>
              </div>
            </div>
            @endfor
          </div>
          <a class="carousel-control-prev" href="#carouselmurali" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselmurali" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modal-back" id="kumar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-gallery">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">

        <div id="carouselkumar" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            @for ($i = 1; $i <= 33; $i++)
            <div class="carousel-item {{($i == 1) ? 'active' : ''}}">
              <img class="d-block w-100" src="{{asset('public/front/images/Kumar/kumar_ssb ('.$i.').jpg')}}" alt="First slide">
              <div class="slider-caption mt-2">
                <h4 style="color: #969696;"> Residence Of Mr. Kumar</h4>
              </div>
            </div>
            @endfor
          </div>
          <a class="carousel-control-prev" href="#carouselkumar" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselkumar" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modal-back" id="ravichandran" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content modal-gallery">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        <div class="modal-body">

          <div id="carouselravichandran" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              @for ($i = 1; $i <= 15; $i++)
              <div class="carousel-item {{($i == 1) ? 'active' : ''}}">
                <img class="d-block w-100" src="{{asset('public/front/images/Ravichandran/Ravichandran('.$i.').JPG')}}" alt="First slide">
                <div class="slider-caption mt-2">
                  <h4 style="color: #969696;"> Residence Of Mr. Ravichandran</h4>
                </div>
              </div>
              @endfor
            </div>
            <a class="carousel-control-prev" href="#carouselravichandran" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselravichandran" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v9.0'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="105584477968961"
theme_color="#f64546"
logged_in_greeting="You can dream, create, design, and build the most wonderful place in the world."
logged_out_greeting="You can dream, create, design, and build the most wonderful place in the world.">
</div>
@endsection
