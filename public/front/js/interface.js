﻿( function($) {
  'use strict';

    /*-------------------------------------------------------------------------------
	  Slider 
	-------------------------------------------------------------------------------*/
	
	if (typeof $.fn.revolution !== 'undefined') {
      
      $("#rev_slider").revolution({
        sliderType:"standard",
        sliderLayout:"fullscreen",
        dottedOverlay:"none",
        delay:7000,
        navigation: {
          keyboardNavigation:"off",
          keyboard_direction: "horizontal",
          onHoverStop:"off",
          touch:{
            touchenabled:"on",
            swipe_threshold: 75,
            swipe_min_touches: 1,
            swipe_direction: "horizontal",
            drag_block_vertical: false
          }
        },
        viewPort: {
          enable:true,
          outof:"pause",
          visible_area:"80%"
        },
        responsiveLevels:[2048,1750,1192],
        gridwidth:[1180,1180,980],
        gridheight:[550],
        lazyType:"none",
        shadow:0,
        spinner:"off",
        stopLoop:"on",
        stopAfterLoops:0,
        shuffle:"off",
        autoHeight:"on",
        fullScreenAlignForce:"off",
        fullScreenOffsetContainer: "",
        fullScreenOffset: "",
        disableProgressBar:"on",
        hideThumbsOnMobile:"off",
        hideSliderAtLimit:0,
        hideCaptionAtLimit:0,
        hideAllCaptionAtLilmit:0,
        debugMode:false,
        fallbacks: {
          simplifyAll:"off",
          nextSlideOnWindowFocus:"off",
          disableFocusListener:false,
        }
      });
    }

  	$('.arrow-left').on('click', function(){
   	    $("#rev_slider").revprev();
  	});
  
	$('.arrow-right').on('click', function(){
	    $("#rev_slider").revnext();
	});

	$('.slide-number .total-count').text($('#rev_slider li').size());

    $('#rev_slider').bind("revolution.slide.onchange",function (e,data) {
	    $('.slide-number .count').text(data.slideIndex);
    });





    /*-------------------------------------------------------------------------------
	  Object Map
	-------------------------------------------------------------------------------*/

	$('.object-label').on('click', function(){
		$('.object-label').not(this).find($('.object-info')).fadeOut(200);
		$(this).find('.object-info').fadeToggle(200);
	});


    /*-------------------------------------------------------------------------------
	  Parallax
	-------------------------------------------------------------------------------*/


	$(window).stellar({
	  	responsive: true,
	  	horizontalScrolling: false,
	  	hideDistantElements: false,
	  	horizontalOffset: 0,
	  	verticalOffset: 0,
	});

	/*-------------------------------------------------------------------------------
	  Project carousel
	-------------------------------------------------------------------------------*/

	$(".js-projects-carousel").owlCarousel({
		itemsMobile:[479,1],
		itemsTablet:[768,2],
		itemsDesktopSmall:[979,2],
		itemsDesktop:[1250,3],
		items:4,
		pagination:false,
		navigation:true,
		slideSpeed:700,
		responsiveRefreshRate:0
	});

	/*-------------------------------------------------------------------------------
	  Gallery
	-------------------------------------------------------------------------------*/



	$('.js-projects-gallery').each(function(){
		$(this).magnificPopup({
			delegate: 'a',
		    type: 'image',
		    removalDelay: 300,
		    tLoading: 'Loading image #%curr%...',
		    gallery: {
		       enabled: true,
		       navigateByImgClick: true,
		       preload:[0,1]
		    },
		    image: {
		       tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		       titleSrc: function(item) {
		          return item.el.attr('title') + '<small></small>';
		       }
		    }

		});
	});

});
